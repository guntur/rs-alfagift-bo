/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.v1.product.process;

import com.alfamart.alfagift_bo.base.exception.ServiceException;
import com.alfamart.alfagift_bo.db.postgresql.ViewMasterAlfamindProductBean;
import com.alfamart.alfagift_bo.v1.product.model.GetProductRequest;
import com.alfamart.alfagift_bo.db.exception.DAOException;
import com.alfamart.alfagift_bo.db.postgresql.ViewMasterAlfamindProductManager;
import com.alfamart.alfagift_bo.v1.base.util.Constant;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.alfamart.alfagift_bo.v1.base.model.Header;
import com.alfamart.alfagift_bo.v1.base.model.Status;
import com.alfamart.alfagift_bo.v1.product.model.Product;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Elko
 */
@Repository
public class ProductProcess {
    
    public Logger logger = LoggerFactory.getLogger(this.getClass());
    private Gson gson = new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd").create();    
    
    public Map<String,Object> getProducts(Header header, GetProductRequest request, int type) {
       
        Status status = new Status();
	Map<String, Object> map = new HashMap<>();
        List<Product> listProduct  = new ArrayList<>();
        logger.info("get Product");
        String plu = request.getPlu();
        String productId = request.getProductId();
        long startUpdateDate=request.getStartUpdateDate();
        long endUpdateDate=request.getEndUpdateDate();
        int startRow=request.getStartRow();
        int limitRow=request.getLimitRow();
        StringBuilder conditionQuery = new StringBuilder();
        try {

            conditionQuery.append("WHERE tbmp_type = "+type);
            if(!ValidatorUtil.isNullOrEmpty(plu)) {

                    logger.info("plu :"+plu);
                    conditionQuery.append(" AND plu = '"+plu+"'");

            }
            if(startUpdateDate> 0 && endUpdateDate>0) {

                    logger.info("startUpdateDate :"+startUpdateDate);
                    logger.info("endUpdateDate :"+endUpdateDate);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    //sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
                    String startUpdate = sdf.format(new Date(startUpdateDate * 1000));
                    String endUpdate = sdf.format(new Date(endUpdateDate * 1000));
                    conditionQuery.append(" AND (tbmp_update_date >= '"+startUpdate+"' " +
                                            "  AND tbmp_update_date <=  '"+endUpdate+"')");

            }
            if(ValidatorUtil.isNullOrEmpty(startRow) && ValidatorUtil.isNullOrEmpty(limitRow)) {

                    startRow=1;
                    limitRow=-1;
            }
            logger.info("startRow :"+startRow);
            logger.info("limitRow :"+limitRow);
            conditionQuery.append(" ORDER by tbmp_update_date ASC ");
            

            ViewMasterAlfamindProductBean[]  productArray = ViewMasterAlfamindProductManager.getInstance().loadByWhere(conditionQuery.toString(),null, startRow, limitRow);

                    if(ValidatorUtil.isNullOrEmpty(productArray) && !ValidatorUtil.isNullOrEmpty(productArray)) {

                             status.setCode("04");
                             status.setMessage("Data PLU ="+plu+ " Not Found");
                        throw new ServiceException(status.getCode(), status.getMessage());
                    }

                    for (ViewMasterAlfamindProductBean product : productArray) {

                            Product productModel = new Product();
                            productModel.setPlu(product.getPlu());
                            productModel.setSkuSeller(product.getTbmpSkuSeller());
                            productModel.setProductName(product.getTbmpName());
                            productModel.setBrand(product.getTbmbName());
                            productModel.setShortDesc(product.getTbmpShortDescription());
                            productModel.setLongdesc(product.getTbmpDescription());
                            productModel.setPrice(product.getTbmpPrice());
                            productModel.setSpecialPrice(product.getTbmpSpecialPrice());
                            productModel.setSpecialStart(product.getTbmpSpecialPriceFrom());
                            productModel.setSpecialEnd(product.getTbmpSpecialPriceTo());
                            productModel.setLength(product.getTbmpLength());
                            productModel.setWidth(product.getTbmpWidth());
                            productModel.setHeight(product.getTbmpWeight());
                            productModel.setWeight(product.getTbmpWeight());
                            productModel.setProductId(product.getTbmpId());
                            productModel.setCreateDate(product.getTbmpCreateDate());
                            productModel.setUpdateDate(product.getTbmpUpdateDate());
                            
                            listProduct.add(productModel);

                    }

                status.setTrxId(header.getTrxId());
                status.setCode(Constant.ResponseCode.SUCCESS);
                status.setMessage(Constant.ResponseMessage.SUCCESS);
                map.put("status", status);
                map.put("product", listProduct);
                
        } catch(ServiceException se){
                status.setCode(se.getCode());
                status.setMessage(se.getMessage());
                logger.error("code : "+status.getCode()+" | message : "+status.getMessage());
                map.put("status", status);
               return map;
          
        }catch (DAOException e) {
                status.setCode(Constant.ResponseCode.DAO_EXCEPTION);
                status.setMessage(e.getMessage());
                logger.error("Get District  Failed - DAO Exception : " + e.getLocalizedMessage());
                map.put("status", status);
                 return map;
        }catch(Exception e){
                e.printStackTrace();
                status.setCode(Constant.ResponseCode.FAILED);
                status.setMessage(Constant.ResponseMessage.FAILED);
                logger.error("Error Get District : "+e.getLocalizedMessage());	
                map.put("status", status);
                return map;
        }
        
         return map;
    }

}
