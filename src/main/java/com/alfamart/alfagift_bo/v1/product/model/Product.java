/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.v1.product.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Elko
 */
public class Product {
    private int productId;
    private String plu;
    private String skuSeller;
    private String productName;
    private String brand;
    private String shortDesc;
    private String longdesc;
    private BigDecimal price;
    private BigDecimal specialPrice;
    private Date specialStart;
    private Date specialEnd;
    private BigDecimal  length;
    private BigDecimal  width;
    private BigDecimal  height;
    private BigDecimal  weight;
    private Date createDate;
    private Date updateDate;
 


    public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getPlu() {
        return plu;
    }

    public void setPlu(String plu) {
        this.plu = plu;
    }

    public String getSkuSeller() {
		return skuSeller;
	}

	public void setSkuSeller(String skuSeller) {
		this.skuSeller = skuSeller;
	}

	public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getLongdesc() {
        return longdesc;
    }

    public void setLongdesc(String longdesc) {
        this.longdesc = longdesc;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(BigDecimal specialPrice) {
        this.specialPrice = specialPrice;
    }

    public Date getSpecialStart() {
        return specialStart;
    }

    public void setSpecialStart(Date specialStart) {
        this.specialStart = specialStart;
    }

    public Date getSpecialEnd() {
        return specialEnd;
    }

    public void setSpecialEnd(Date specialEnd) {
        this.specialEnd = specialEnd;
    }

    public BigDecimal getLength() {
        return length;
    }

    public void setLength(BigDecimal length) {
        this.length = length;
    }

    public BigDecimal getWidth() {
        return width;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

}
