/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.v1.product.model;

import javax.ws.rs.QueryParam;

/**
 *
 * @author Elko
 */
public class GetProductRequest {
    @QueryParam("productId")
    private String productId;
    @QueryParam("plu")
    private String plu;
    @QueryParam("startUpdateDate")
    private long startUpdateDate;
    @QueryParam("endUpdateDate")
    private long endUpdateDate;
    @QueryParam("startRow")
    private int startRow;
    @QueryParam("limitRow")
    private int limitRow;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPlu() {
        return plu;
    }

    public void setPlu(String plu) {
        this.plu = plu;
    }

    public long getStartUpdateDate() {
        return startUpdateDate;
    }

    public void setStartUpdateDate(long startUpdateDate) {
        this.startUpdateDate = startUpdateDate;
    }

    public long getEndUpdateDate() {
        return endUpdateDate;
    }

    public void setEndUpdateDate(long endUpdateDate) {
        this.endUpdateDate = endUpdateDate;
    }

    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getLimitRow() {
        return limitRow;
    }

    public void setLimitRow(int limitRow) {
        this.limitRow = limitRow;
    }
    
    
}
