package com.alfamart.alfagift_bo.v1.config.auth;


import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alfamart.alfagift_bo.v1.base.util.Constant;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class TokenAuthentication {
	
	static Logger log = LoggerFactory.getLogger(TokenAuthentication.class);
	
    public static Claims validateToken(String token){
    	Claims claims = null;
    	try {
    		 claims = Jwts.parser()
                     	.setSigningKey(Constant.Token.APPLICATION_SECRET)
                     		.parseClaimsJws(token)
                     			.getBody();
		} catch (ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException | IllegalArgumentException e) {
			return claims;
		}
       
        return claims;
    }

    public static String generateToken(String memId, String emailId, String applicationSecret) {
        String token = Jwts.builder()
        		//set header
        		.setId(memId)
        		.setHeaderParam("typ", "JWT")
        		.setHeaderParam("alg", "HS256")
        		
        		//set payload or claim
                .setSubject(emailId)
                .setIssuer(memId)
                 
                //set signature
                .signWith(SignatureAlgorithm.HS256, applicationSecret)
                .compact();
        return token;
    }
    
    public static String generateTokenForFogotPassword(String memId, String emailId, String applicationSecret) {
        String token = Jwts.builder()
        		//set header
        		.setId(memId)
        		.setHeaderParam("typ", "JWT")
        		.setHeaderParam("alg", "HS256")
        		
        		//set payload or claim
                .setSubject(emailId)
                .setIssuer(memId)
                .setExpiration(new Date(System.currentTimeMillis()+60*60*24*1000))
                 
                //set signature
                .signWith(SignatureAlgorithm.HS256, applicationSecret)
                .compact();
        return token;
    }
    
    public static void main(String args[]){
    	String token = "";
    	
    	token = generateToken("5b60c24f2042cc03b11fde0a", "alf@gi.ft", Constant.Token.APPLICATION_SECRET);
    	System.out.println("Hasil generate token = "+token);
    	
        Claims claims = validateToken(token);
		System.out.println("ID: " + claims.getId());
		System.out.println("Subject: " + claims.getSubject());
		System.out.println("Issuer: " + claims.getIssuer());
		System.out.println("Expiration: " + claims.getExpiration());
    }

}
