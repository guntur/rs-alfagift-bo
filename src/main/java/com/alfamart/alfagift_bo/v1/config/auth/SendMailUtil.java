/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.v1.config.auth;

import com.alfamart.alfagift_bo.v1.base.util.Properties;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;

import java.io.File;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Elko
 */
public class SendMailUtil {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    public boolean sendEmail(String from, String to, String subject, String text, String pathAttachment, String nameAttachment) throws MessagingException {
		boolean ret = false;

		final String username = Properties.getString("blast.mail.user");
		final String password = Properties.getString("blast.mail.key");

		java.util.Properties props = new java.util.Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host",  Properties.getString("blast.mail.host"));
		props.put("mail.smtp.port", Properties.getString("blast.mail.port"));

		Session session = Session.getInstance(
				props, new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				}
		);

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
		message.setSubject(subject);

		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText(text, "utf-8", "html");
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		
		if (!ValidatorUtil.isNullOrEmpty(pathAttachment) && !ValidatorUtil.isNullOrEmpty(nameAttachment)) {
			if (new File(pathAttachment).exists()) {
				messageBodyPart = new MimeBodyPart();
		        DataSource source = new FileDataSource(pathAttachment);
		        messageBodyPart.setDataHandler(new DataHandler(source));
		        messageBodyPart.setFileName(nameAttachment);
		        multipart.addBodyPart(messageBodyPart);
			}
		}
		
		message.setContent(multipart);

		Transport.send(message);

		ret = true;

		return ret;
	}
    
    public static void main(String args[]) throws AddressException, MessagingException {
		System.out.println("--------test--------");	
                
		SendMailUtil util =  new SendMailUtil();

		Boolean anuan = util.sendEmail(
				"noreply@alfacart.com",
				"guntur.teguh@wegeeks.co",
				"Email test",
				"Test email aja nih",
				"/home/zer0/Documents/rahasia/PROJECT_ALFA_GIFT/SOURCE/STAGING/alfagift_media/Promo/Brand/pm_brand_180727_Q1XZ.jpg",
				"jangan di download"
		);

		System.out.println("email terkirim : " + anuan);
		System.out.println("-------------End of test-------------");
    }
    
    
}
