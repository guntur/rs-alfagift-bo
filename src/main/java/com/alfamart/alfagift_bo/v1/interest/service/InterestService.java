package com.alfamart.alfagift_bo.v1.interest.service;

import com.alfamart.alfagift_bo.base.exception.ServiceException;
import com.alfamart.alfagift_bo.db.mongo.domain.Interest;
import com.alfamart.alfagift_bo.db.mongo.repository.InterestRepository;
import com.alfamart.alfagift_bo.v1.base.model.Status;
import com.alfamart.alfagift_bo.v1.base.util.ClassUtil;
import com.alfamart.alfagift_bo.v1.base.util.Constant;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;
import com.alfamart.alfagift_bo.v1.interest.model.InterestModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.MongoException;
import org.apache.commons.codec.digest.DigestUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by zer0, the Maverick Hunter
 * on 03/08/18.
 * Class: StoreLocatorProcess.java
 */
@Service
public class InterestService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private final MongoTemplate mongoTemplate;
    private final InterestRepository interestRepository;

    @Autowired
    public InterestService(MongoTemplate mongoTemplate, InterestRepository interestRepository) {
        this.mongoTemplate = mongoTemplate;
        this.interestRepository = interestRepository;
    }

    public List<InterestModel> getMasterInterest() throws ServiceException {

        List<InterestModel> interestModels = new ArrayList<>();

        try {

            // for bo, find all regardless of its status
            List<Interest> interests = interestRepository.findAll();

            if (interests.size() > 0) {

                for (Interest interest : interests) {
                    interestModels.add(new InterestModel(interest));
                }

            }

        } catch (MongoException e) {
            logger.error("ALFAGIFT - getMasterInterest() error : with message : " + e.getCode() + " : " + e.getMessage());
            throw new ServiceException(Constant.ResponseCode.EXCEPTION, Constant.ResponseMessage.GLOBAL_ERROR);
        }

        return interestModels;
    }

    public InterestModel getInterest(String id) {

        InterestModel interestModel = null;

        try {

            Optional<Interest> interest = interestRepository.findById(id);

            if (interest.isPresent()) {
                interestModel = new InterestModel(interest.get());
            }

        } catch (MongoException e) {
            ClassUtil.getInstance().logError(logger, e);
        }


        return interestModel;
    }
}
