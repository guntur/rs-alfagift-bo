package com.alfamart.alfagift_bo.v1.interest.model;


import com.alfamart.alfagift_bo.db.mongo.domain.Interest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by zer0, the Maverick Hunter
 * on 06/08/18.
 * Class: Interest.java
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InterestModel implements Serializable {

    private String title;
    private String webImagePath;
    private String appImagePath;
    private Integer status;
    private Integer createdBy;
    private Integer updatedBy;
    private String createdDate;
    private String updatedDate;

    public InterestModel(Interest interest) {
        this.title = interest.getTitle();
        this.webImagePath = interest.getWebImagePath();
        this.appImagePath = interest.getAppImagePath();
        this.status = interest.getStatus();
        this.createdBy = interest.getCreatedBy();
        this.createdDate = interest.getCreatedAt().toString();

        if (interest.getUpdatedBy() != null) {
            this.updatedBy = interest.getUpdatedBy();
        }

        if (interest.getUpdatedAt() != null) {
            this.updatedDate = interest.getUpdatedAt().toString();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebImagePath() {
        return webImagePath;
    }

    public void setWebImagePath(String webImagePath) {
        this.webImagePath = webImagePath;
    }

    public String getAppImagePath() {
        return appImagePath;
    }

    public void setAppImagePath(String appImagePath) {
        this.appImagePath = appImagePath;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        Date date;
        String newDateString = null;
        try {
            date = sdf.parse(createdDate);

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));
            newDateString = sdf2.format(date);
        } catch(ParseException e) {
            e.printStackTrace();
        }

        this.createdDate = newDateString;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        Date date;
        String newDateString = null;
        try {
            date = sdf.parse(updatedDate);

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));
            newDateString = sdf2.format(date);
        } catch(ParseException e) {
            e.printStackTrace();
        }

        this.updatedDate = newDateString;
    }
}
