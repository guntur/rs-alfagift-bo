package com.alfamart.alfagift_bo.v1.promotion.service;

import com.alfamart.alfagift_bo.db.mongo.domain.Brand;
import com.alfamart.alfagift_bo.db.mongo.domain.Campaign;
import com.alfamart.alfagift_bo.db.mongo.domain.ExtraPoint;
import com.alfamart.alfagift_bo.db.mongo.domain.Reward;
import com.alfamart.alfagift_bo.db.mongo.repository.BrandRepository;
import com.alfamart.alfagift_bo.db.mongo.repository.CampaignRepository;
import com.alfamart.alfagift_bo.db.mongo.repository.ExtraPointRepository;
import com.alfamart.alfagift_bo.db.mongo.repository.RewardRepository;
import com.alfamart.alfagift_bo.v1.base.util.AlfagiftUtil;
import com.alfamart.alfagift_bo.v1.base.util.ClassUtil;
import com.alfamart.alfagift_bo.v1.base.util.Properties;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;
import com.alfamart.alfagift_bo.v1.promotion.model.BrandPromotion;
import com.alfamart.alfagift_bo.v1.promotion.model.CampaignPromotion;
import com.alfamart.alfagift_bo.v1.promotion.model.ExtraPointPromotion;
import com.alfamart.alfagift_bo.v1.promotion.model.RewardPromotion;
import com.mongodb.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by zer0, the Maverick Hunter
 * on 27/07/18.
 * Class: PromotionManagementService.java
 */
@Service(value = "alfagiftPromotionManagementService")
public class PromotionManagementService {

    private Logger log = LoggerFactory.getLogger("bov2");
    private AlfagiftUtil alfagiftUtil = new AlfagiftUtil();

    private final BrandRepository brandRepository;
    private final CampaignRepository campaignRepository;
    private final RewardRepository rewardRepository;
    private final ExtraPointRepository extraPointRepository;

    @Autowired
    public PromotionManagementService(
            BrandRepository brandRepository,
            CampaignRepository campaignRepository,
            RewardRepository rewardRepository,
            ExtraPointRepository extraPointRepository
    ) {
        this.brandRepository = brandRepository;
        this.campaignRepository = campaignRepository;
        this.rewardRepository = rewardRepository;
        this.extraPointRepository = extraPointRepository;
    }

    // Method BRAND
    public Brand saveUpdateBrand(BrandPromotion brandDTO, Integer userId) {
        Boolean saved = false;
        String prefixUpload="pm_brand_";
        Brand brand;

        if (brandDTO.getBrandId().isEmpty()) {
            brand = new Brand();
            brand.setCreatedBy(userId);
            brand.setCreatedAt(new Date());
            brand.setUpdatedBy(null);
            brand.setUpdatedAt(null);
        } else {
            Optional<Brand> optBrand = brandRepository.findById(brandDTO.getBrandId());
            brand = optBrand.get();
            brand.setUpdatedBy(userId);
            brand.setUpdatedAt(new Date());
        }

        brand.setBrandName(brandDTO.getBrandName());
        brand.setSortNo(brandDTO.getSortNo());
        brand.setStatus(brandDTO.getStatus());
        brand.setImagePath(brandDTO.getImagePath());

        try {
            brandRepository.save(brand);
            saved = true;
        } catch (MongoException e) {
            ClassUtil.getInstance().logError(log, e);
        }

        // do or not do redis ?

        return brand;
    }

    public List<BrandPromotion> getBrandList() {

        List<BrandPromotion> brandPromotions = new ArrayList<>();

        try {

            List<Brand> brands = brandRepository.findAll();

            if (brands.size() > 0) {
                for (Brand brand : brands) {
                    BrandPromotion brandPromotion = new BrandPromotion();
                    brandPromotion.setBrandId(brand.getId());
                    brandPromotion.setBrandName(brand.getBrandName());
                    brandPromotion.setSortNo(brand.getSortNo());
                    brandPromotion.setTotalCampaign(0);
                    brandPromotion.setStatus(brand.getStatus());
                    brandPromotion.setCreatedBy(brand.getCreatedBy());

                    if (brand.getUpdatedBy() != null) {
                        brandPromotion.setUpdatedBy(brand.getUpdatedBy());
                    } else {
                        brandPromotion.setUpdatedBy(null);
                    }

                    brandPromotion.setCreatedDate(brand.getCreatedAt().toString());

                    if (brand.getUpdatedAt() != null) {
                        brandPromotion.setUpdatedDate(brand.getUpdatedAt().toString());
                    } else {
                        brandPromotion.setUpdatedBy(null);
                    }

                    brandPromotions.add(brandPromotion);
                }
            }
        } catch (MongoException e) {
            throw new MongoException(e.getCode(), e.getMessage());
        }

        return brandPromotions;
    }

    public BrandPromotion getBrand(String id) {

        BrandPromotion brandPromotion = null;

        try {

            Optional<Brand> brand = brandRepository.findById(id);

            if (brand.isPresent()) {
                brandPromotion = new BrandPromotion(brand.get());
            }

        } catch (MongoException e) {
            ClassUtil.getInstance().logError(log, e);
        }


        return brandPromotion;

    }

    // Method CAMPAIGN
    public Boolean saveUpdateCampaign(CampaignPromotion campaignDTO, Integer userId) {
        Boolean saved = false;
        Campaign campaign;

        if (campaignDTO.getCampaignId().isEmpty()) {
            campaign = new Campaign();
            campaign.setCreatedBy(userId);
            campaign.setCreatedAt(new Date());
            campaign.setUpdatedBy(null);
            campaign.setUpdatedAt(null);
        } else {
            Optional<Campaign> tmpCamp = campaignRepository.findById(campaignDTO.getCampaignId());
            campaign = tmpCamp.get();
            campaign.setUpdatedBy(userId);
            campaign.setUpdatedAt(new Date());
        }

        campaign.setBrandId(campaignDTO.getBrandId());
        campaign.setCampaignName(campaignDTO.getCampaignName());
        campaign.setCampaignDescription(campaignDTO.getCampaignDescription());
        campaign.setChanelProgram(campaignDTO.getChanelProgram());
        campaign.setWebUrl(campaignDTO.getWebUrl());
        campaign.setStartDate(campaignDTO.getStartDate());
        campaign.setStartTime(campaignDTO.getStartTime());
        campaign.setEndDate(campaignDTO.getEndDate());
        campaign.setEndTime(campaignDTO.getEndTime());
        campaign.setStatus(campaignDTO.getStatus());
        campaign.setSort(campaignDTO.getSort());

        try {
            campaignRepository.save(campaign);
            saved = true;
        } catch (MongoException e) {
            ClassUtil.getInstance().logError(log, e);
        }

        // do or not do redis ?

        return saved;
    }

    public List<CampaignPromotion> getCampaignList() {

        List<CampaignPromotion> campaignPromotions = new ArrayList<>();
        List<Campaign> campaigns = campaignRepository.findAll();

        if (campaigns.size() > 0) {
            for (Campaign campaign : campaigns) {
                CampaignPromotion campaignPromotion = new CampaignPromotion();
                campaignPromotion.setCampaignId(campaign.getId());
                campaignPromotion.setBrandId(campaign.getBrandId());
                campaignPromotion.setCampaignName(campaign.getCampaignName());
                campaignPromotion.setCampaignDescription(campaign.getCampaignDescription());
                campaignPromotion.setChanelProgram(campaign.getChanelProgram());
                campaignPromotion.setWebUrl(campaign.getWebUrl());
                campaignPromotion.setStartDate(campaign.getStartDate());
                campaignPromotion.setStartTime(campaign.getStartTime());
                campaignPromotion.setEndDate(campaign.getEndDate());
                campaignPromotion.setEndTime(campaign.getEndTime());
                campaignPromotion.setSort(campaign.getSort());
                campaignPromotion.setStatus(campaign.getStatus());
                campaignPromotion.setCreatedBy(campaign.getCreatedBy());

                if (campaign.getUpdatedBy() != null) {
                    campaignPromotion.setUpdatedBy(campaign.getUpdatedBy());
                } else {
                    campaignPromotion.setUpdatedBy(null);
                }

                campaignPromotion.setCreatedDate(campaign.getCreatedAt().toString());

                if (campaign.getUpdatedAt() != null) {
                    campaignPromotion.setUpdatedDate(campaign.getUpdatedAt().toString());
                } else {
                    campaignPromotion.setUpdatedBy(null);
                }

                campaignPromotions.add(campaignPromotion);
            }
        }

        return campaignPromotions;
    }

    public CampaignPromotion getCampaign(String id) {
        CampaignPromotion campaignPromotion = null;
        try {
            Optional<Campaign> campaign = campaignRepository.findById(id);

            if (campaign.isPresent()) {
                campaignPromotion = new CampaignPromotion(campaign.get());
            }

        } catch (MongoException e) {
            ClassUtil.getInstance().logError(log, e);
        }

        return campaignPromotion;
    }

    // Method REWARD
    public Boolean saveUpdateReward(RewardPromotion rewardPointDTO, Integer userId) {
        Boolean saved = false;
        String prefixUploadWeb="pm_reward_web_";
        String prefixUploadApp="pm_reward_app_";
        String prefixUploadXls="pm_reward_xls";
        Reward reward;

        if (rewardPointDTO.getRewardId().isEmpty()) {
            reward = new Reward();
            reward.setCreatedBy(userId);
            reward.setCreatedAt(new Date());
            reward.setUpdatedBy(null);
            reward.setUpdatedAt(null);
        } else {
            Optional<Reward> rewardPromotion = rewardRepository.findById(rewardPointDTO.getRewardId());
            reward = rewardPromotion.get();
            reward.setUpdatedBy(userId);
            reward.setUpdatedAt(new Date());
        }

        reward.setRewardName(rewardPointDTO.getRewardName());
        reward.setCampaignId(rewardPointDTO.getCampaignId());
        reward.setCategoryId(rewardPointDTO.getCategoryId());
        reward.setRewardDescription(rewardPointDTO.getRewardDescription());
        reward.setRewardEndDate(rewardPointDTO.getRewardEndDate());
        reward.setRewardTermCond(rewardPointDTO.getRewardTermCond());
        reward.setStatus(rewardPointDTO.getStatus());
        reward.setVoucherLabel(rewardPointDTO.getVoucherLabel());
        reward.setVoucherClaim(rewardPointDTO.getVoucherClaim());
        reward.setVoucherClaimType(rewardPointDTO.getVoucherClaimType());
        reward.setVoucherUser(rewardPointDTO.getVoucherUser());
        reward.setVoucherFileXls(rewardPointDTO.getVoucherFileXls());
        reward.setVoucherFileXlsPath(rewardPointDTO.getVoucherFileXlsPath());

        if (!ValidatorUtil.isNullOrEmpty(rewardPointDTO.getWebImage()) && !ValidatorUtil.isNullOrEmpty(rewardPointDTO.getWebImage().getOriginalFilename())) {
            try {
                String path = Properties.getString("params.alfagift.reward.web.image");
                String linkUpload = alfagiftUtil.uploadImage(prefixUploadWeb, rewardPointDTO.getWebImage(), path);
                reward.setWebImagePath(linkUpload);
            } catch (IOException | ServletException e) {
                ClassUtil.getInstance().logError(log, e);
            }
        } else {
            reward.setWebImagePath(ValidatorUtil.isNullOrEmpty(rewardPointDTO.getWebImagePath()) ? null : rewardPointDTO.getWebImagePath());
        }

        if (!ValidatorUtil.isNullOrEmpty(rewardPointDTO.getAppImage()) && !ValidatorUtil.isNullOrEmpty(rewardPointDTO.getAppImage().getOriginalFilename())) {
            try {
                String path = Properties.getString("params.alfagift.reward.app.image");
                String linkUpload = alfagiftUtil.uploadImage(prefixUploadApp, rewardPointDTO.getAppImage(), path);
                reward.setAppImagePath(linkUpload);
            } catch (IOException | ServletException e) {
                ClassUtil.getInstance().logError(log, e);
            }
        } else {
            reward.setAppImagePath(ValidatorUtil.isNullOrEmpty(rewardPointDTO.getAppImagePath()) ? null : rewardPointDTO.getAppImagePath());
        }

        if (!ValidatorUtil.isNullOrEmpty(rewardPointDTO.getVoucherFileXls())) {
            try {
                String path = Properties.getString("params.alfagift.reward.xls");
                String linkUpload = alfagiftUtil.uploadImage(prefixUploadXls, rewardPointDTO.getVoucherFileXls(), path);
                reward.setVoucherFileXlsPath(linkUpload);
            } catch (IOException | ServletException e) {
                ClassUtil.getInstance().logError(log, e);
            }
        } else {
            reward.setVoucherFileXlsPath(ValidatorUtil.isNullOrEmpty(rewardPointDTO.getVoucherFileXlsPath()) ? null : rewardPointDTO.getVoucherFileXlsPath());
        }

        try {
            rewardRepository.save(reward);
            saved = true;
        } catch (MongoException e) {
            ClassUtil.getInstance().logError(log, e);
        }

        // do or not do redis ?

        return saved;
    }

    public List<RewardPromotion> getRewardList() {

        List<RewardPromotion> rewardPromotions = new ArrayList<>();
        List<Reward> rewards = rewardRepository.findAll();

        if (rewards.size() > 0) {
            for (Reward reward : rewards) {
                RewardPromotion rewardPromotion = new RewardPromotion();
                rewardPromotion.setRewardId(reward.getId());
                rewardPromotion.setRewardName(reward.getRewardName());
                rewardPromotion.setCampaignId(reward.getCampaignId());
                rewardPromotion.setCategoryId(reward.getCategoryId());
                rewardPromotion.setRewardDescription(reward.getRewardDescription());
                rewardPromotion.setRewardEndDate(reward.getRewardEndDate());
                rewardPromotion.setRewardTermCond(reward.getRewardTermCond());
                rewardPromotion.setWebImage(reward.getWebImage());
                rewardPromotion.setAppImage(reward.getAppImage());
                rewardPromotion.setStatus(reward.getStatus());
                rewardPromotion.setVoucherLabel(reward.getVoucherLabel());
                rewardPromotion.setVoucherClaim(reward.getVoucherClaim());
                rewardPromotion.setVoucherUser(reward.getVoucherUser());
                rewardPromotion.setVoucherFileXls(reward.getVoucherFileXls());
                rewardPromotion.setVoucherFileXlsPath(reward.getVoucherFileXlsPath());

                rewardPromotion.setCreatedBy(reward.getCreatedBy());
                if (reward.getUpdatedBy() != null) {
                    rewardPromotion.setUpdatedBy(reward.getUpdatedBy());
                } else {
                    rewardPromotion.setUpdatedBy(null);
                }

                rewardPromotion.setCreatedDate(reward.getCreatedAt().toString());
                if (reward.getUpdatedAt() != null) {
                    rewardPromotion.setUpdatedDate(reward.getUpdatedAt().toString());
                } else {
                    rewardPromotion.setUpdatedBy(null);
                }

                rewardPromotions.add(rewardPromotion);
            }
        }

        return rewardPromotions;
    }

    public RewardPromotion getReward(String id) {
        RewardPromotion rewardPromotion = null;
        try {
            Optional<Reward> reward = rewardRepository.findById(id);
            if (reward.isPresent()) {
                rewardPromotion = new RewardPromotion(reward.get());
            }
        } catch (MongoException e) {
            ClassUtil.getInstance().logError(log, e);
        }
        return rewardPromotion;
    }

    // Method EXTRA POINT
    public Boolean saveUpdateExtraPoint(ExtraPointPromotion extraPointDTO, Integer userId) {
        Boolean saved = false;
        ExtraPoint extraPoint;

        if (extraPointDTO.getExtraPointId().isEmpty()) {
            extraPoint = new ExtraPoint();
            extraPoint.setCreatedBy(userId);
            extraPoint.setCreatedAt(new Date());
            extraPoint.setUpdatedBy(null);
            extraPoint.setUpdatedAt(null);
        } else {
            Optional<ExtraPoint> extraPoint1 = extraPointRepository.findById(extraPointDTO.getExtraPointId());
            extraPoint = extraPoint1.get();
            extraPoint.setUpdatedBy(userId);
            extraPoint.setUpdatedAt(new Date());
        }

        extraPoint.setExtraPointTitle(extraPointDTO.getExtraPointTitle());
        extraPoint.setCampaignId(extraPointDTO.getCampaignId());
        extraPoint.setAlfaStar(extraPointDTO.getAlfaStar());
        extraPoint.setPontaPoint(extraPointDTO.getPontaPoint());
        extraPoint.setStatus(extraPointDTO.getStatus());

        try {
            extraPointRepository.save(extraPoint);
            saved = true;
        } catch (MongoException e) {
            ClassUtil.getInstance().logError(log, e);
        }

        // do or not do redis ?

        return saved;
    }

    public List<ExtraPointPromotion> getExtraPointList() {

        List<ExtraPointPromotion> extraPointPromotions = new ArrayList<>();
        List<ExtraPoint> extraPoints = extraPointRepository.findAll();

        if (extraPoints.size() > 0) {
            for (ExtraPoint extraPoint : extraPoints) {
                ExtraPointPromotion extraPointPromotion = new ExtraPointPromotion();
                extraPointPromotion.setExtraPointId(extraPoint.getId());
                extraPointPromotion.setExtraPointTitle(extraPoint.getExtraPointTitle());
                extraPointPromotion.setCampaignId(extraPoint.getCampaignId());
                extraPointPromotion.setAlfaStar(extraPoint.getAlfaStar());
                extraPointPromotion.setPontaPoint(extraPoint.getPontaPoint());
                extraPointPromotion.setStatus(extraPoint.getStatus());
                extraPointPromotion.setCreatedBy(extraPoint.getCreatedBy());

                if (extraPoint.getUpdatedBy() != null) {
                    extraPointPromotion.setUpdatedBy(extraPoint.getUpdatedBy());
                } else {
                    extraPointPromotion.setUpdatedBy(null);
                }

                extraPointPromotion.setCreatedDate(extraPoint.getCreatedAt().toString());

                if (extraPoint.getUpdatedAt() != null) {
                    extraPointPromotion.setUpdatedDate(extraPoint.getUpdatedAt().toString());
                } else {
                    extraPointPromotion.setUpdatedBy(null);
                }

                extraPointPromotions.add(extraPointPromotion);
            }
        }
        return extraPointPromotions;
    }

    public ExtraPointPromotion getExtraPoint(String id) {
        ExtraPointPromotion extraPointPromotion = null;
        try {
            Optional<ExtraPoint> extraPoint = extraPointRepository.findById(id);
            if (extraPoint.isPresent()) {
                extraPointPromotion = new ExtraPointPromotion(extraPoint.get());
            }
        } catch (MongoException e) {
            ClassUtil.getInstance().logError(log, e);
        }
        return extraPointPromotion;
    }
}
