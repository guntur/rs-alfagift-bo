package com.alfamart.alfagift_bo.v1.promotion.model;

import com.alfamart.alfagift_bo.db.mongo.domain.Reward;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class RewardPromotion implements Serializable {
    private String rewardId;
    private String rewardName;
    private String campaignId;
    private String categoryId;
    private String rewardEndDate;
    private String rewardDescription;
    private String rewardTermCond;
    private MultipartFile webImage;
    private String webImagePath;
    private MultipartFile appImage;
    private String appImagePath;
    private Integer status;
    private String voucherLabel;
    private String voucherClaim;
    private String voucherClaimType;
    private String voucherUser;
    private MultipartFile voucherFileXls;
    private String voucherFileXlsPath;
    private Integer createdBy;
    private Integer updatedBy;
    private String createdDate;
    private String updatedDate;

    public RewardPromotion(){
    }

    public RewardPromotion(Reward reward){
        DateFormat df = new SimpleDateFormat("M dd, yyyy hh:mm:ss p");

        this.rewardId = reward.getId();
        this.rewardName = reward.getRewardName();
        this.campaignId = reward.getCampaignId();
        this.categoryId = reward.getCategoryId();
        this.rewardEndDate = reward.getRewardEndDate();
        this.rewardDescription = reward.getRewardDescription();
        this.rewardTermCond = reward.getRewardTermCond();
        this.webImage = reward.getWebImage();
        this.webImagePath = reward.getWebImagePath();
        this.appImage = reward.getAppImage();
        this.appImagePath = reward.getAppImagePath();
        this.status = reward.getStatus();
        this.voucherLabel = reward.getVoucherLabel();
        this.voucherClaim = reward.getVoucherClaim();
        this.voucherClaimType = reward.getVoucherClaimType();
        this.voucherUser = reward.getVoucherUser();
        this.voucherFileXls = reward.getVoucherFileXls();
        this.voucherFileXlsPath = reward.getVoucherFileXlsPath();

        if (reward.getUpdatedBy() != null) {
            this.updatedBy = reward.getUpdatedBy();
        }

        this.createdDate = df.format(reward.getCreatedAt());

        if (reward.getUpdatedAt() != null) {
            this.updatedDate = df.format(reward.getUpdatedAt());
        }

        this.createdBy = reward.getCreatedBy();
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getRewardEndDate() {
        return rewardEndDate;
    }

    public void setRewardEndDate(String rewardEndDate) {
        this.rewardEndDate = rewardEndDate;
    }

    public String getRewardDescription() {
        return rewardDescription;
    }

    public void setRewardDescription(String rewardDescription) {
        this.rewardDescription = rewardDescription;
    }

    public String getRewardTermCond() {
        return rewardTermCond;
    }

    public void setRewardTermCond(String rewardTermCond) {
        this.rewardTermCond = rewardTermCond;
    }

    public MultipartFile getWebImage() {
        return webImage;
    }

    public void setWebImage(MultipartFile webImage) {
        this.webImage = webImage;
    }

    public String getWebImagePath() {
        return webImagePath;
    }

    public void setWebImagePath(String webImagePath) {
        this.webImagePath = webImagePath;
    }

    public MultipartFile getAppImage() {
        return appImage;
    }

    public void setAppImage(MultipartFile appImage) {
        this.appImage = appImage;
    }

    public String getAppImagePath() {
        return appImagePath;
    }

    public void setAppImagePath(String appImagePath) {
        this.appImagePath = appImagePath;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getVoucherLabel() {
        return voucherLabel;
    }

    public void setVoucherLabel(String voucherLabel) {
        this.voucherLabel = voucherLabel;
    }

    public String getVoucherClaim() {
        return voucherClaim;
    }

    public void setVoucherClaim(String voucherClaim) {
        this.voucherClaim = voucherClaim;
    }

    public String getVoucherClaimType() {
        return voucherClaimType;
    }

    public void setVoucherClaimType(String voucherClaimType) {
        this.voucherClaimType = voucherClaimType;
    }

    public String getVoucherUser() {
        return voucherUser;
    }

    public void setVoucherUser(String voucherUser) {
        this.voucherUser = voucherUser;
    }

    public MultipartFile getVoucherFileXls() {
        return voucherFileXls;
    }

    public void setVoucherFileXls(MultipartFile voucherFileXls) {
        this.voucherFileXls = voucherFileXls;
    }

    public String getVoucherFileXlsPath() {
        return voucherFileXlsPath;
    }

    public void setVoucherFileXlsPath(String voucherFileXlsPath) {
        this.voucherFileXlsPath = voucherFileXlsPath;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        Date date;
        String newDateString = null;
        try {
            date = sdf.parse(createdDate);

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));
            newDateString = sdf2.format(date);
        } catch(ParseException e) {
            e.printStackTrace();
        }

        this.createdDate = newDateString;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        Date date;
        String newDateString = null;
        try {
            date = sdf.parse(updatedDate);

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));
            newDateString = sdf2.format(date);
        } catch(ParseException e) {
            e.printStackTrace();
        }

        this.updatedDate = newDateString;
    }
}
