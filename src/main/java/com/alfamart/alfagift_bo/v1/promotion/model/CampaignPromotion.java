package com.alfamart.alfagift_bo.v1.promotion.model;

import com.alfamart.alfagift_bo.db.mongo.domain.Campaign;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class CampaignPromotion implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6816217153803306731L;
	
	private String campaignId;
    private String brandId;
    private String campaignName;
    private String campaignDescription;
    private String chanelProgram;
    private String webUrl;
    private String startDate;
    private String startTime;
    private String endDate;
    private String endTime;
    private Integer status;
    private Integer sort;
    private Integer createdBy;
    private Integer updatedBy;
    private String createdDate;
    private String updatedDate;

    public CampaignPromotion(){
    }

    public CampaignPromotion(Campaign campaign){
        this.campaignId = campaign.getId();
        this.brandId = campaign.getBrandId();
        this.campaignName = campaign.getCampaignName();
        this.campaignDescription = campaign.getCampaignDescription();
        this.chanelProgram = campaign.getChanelProgram();
        this.webUrl = campaign.getWebUrl();
        this.startDate = campaign.getStartDate();
        this.startTime = campaign.getStartTime();
        this.endDate = campaign.getEndDate();
        this.endTime = campaign.getEndTime();
        this.sort = campaign.getSort();
        this.status = campaign.getStatus();

        if (campaign.getUpdatedBy() != null) {
            this.updatedBy = campaign.getUpdatedBy();
        }

        this.createdDate = campaign.getCreatedAt().toString();

        if (campaign.getUpdatedAt() != null) {
            this.updatedDate = campaign.getUpdatedAt().toString();
        }

        this.createdBy = campaign.getCreatedBy();
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getCampaignDescription() {
        return campaignDescription;
    }

    public void setCampaignDescription(String campaignDescription) {
        this.campaignDescription = campaignDescription;
    }

    public String getChanelProgram() {
        return chanelProgram;
    }

    public void setChanelProgram(String chanelProgram) {
        this.chanelProgram = chanelProgram;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
    	SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		Date date;
		String newDateString = null;
		try {
			date = sdf.parse(createdDate);
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));
			newDateString = sdf2.format(date);
		} catch(ParseException e) {
			e.printStackTrace();
		}
    	
    	this.createdDate = newDateString;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
    	SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		Date date;
		String newDateString = null;
		try {
			date = sdf.parse(updatedDate);
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));
			newDateString = sdf2.format(date);
		} catch(ParseException e) {
			e.printStackTrace();
		}
    	
    	this.updatedDate = newDateString;
    }
}
