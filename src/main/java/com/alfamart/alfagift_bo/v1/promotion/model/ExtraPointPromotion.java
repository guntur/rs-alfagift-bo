package com.alfamart.alfagift_bo.v1.promotion.model;

import com.alfamart.alfagift_bo.db.mongo.domain.ExtraPoint;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class ExtraPointPromotion implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 3645314804807360964L;
	
	private String extraPointId;
    private String extraPointTitle;
    private String campaignId;
    private String pontaPoint;
    private Integer alfaStar;
    private Integer status;
    private Integer createdBy;
    private Integer updatedBy;
    private String createdDate;
    private String updatedDate;

    public ExtraPointPromotion(){
    }

    public ExtraPointPromotion(ExtraPoint extraPoint){
        this.extraPointId = extraPoint.getId();
        this.extraPointTitle = extraPoint.getExtraPointTitle();
        this.campaignId = extraPoint.getCampaignId();
        this.pontaPoint = extraPoint.getPontaPoint();
        this.alfaStar = extraPoint.getAlfaStar();
        this.status = extraPoint.getStatus();

        if (extraPoint.getUpdatedBy() != null) {
            this.updatedBy = extraPoint.getUpdatedBy();
        }

        this.createdDate = extraPoint.getCreatedAt().toString();

        if (extraPoint.getUpdatedAt() != null) {
            this.updatedDate = extraPoint.getUpdatedAt().toString();
        }

        this.createdBy = extraPoint.getCreatedBy();
    }

    public String getExtraPointId() {
        return extraPointId;
    }

    public void setExtraPointId(String extraPointId) {
        this.extraPointId = extraPointId;
    }

    public String getExtraPointTitle() {
        return extraPointTitle;
    }

    public void setExtraPointTitle(String extraPointTitle) {
        this.extraPointTitle = extraPointTitle;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getPontaPoint() {
        return pontaPoint;
    }

    public void setPontaPoint(String pontaPoint) {
        this.pontaPoint = pontaPoint;
    }

    public Integer getAlfaStar() {
        return alfaStar;
    }

    public void setAlfaStar(Integer alfaStar) {
        this.alfaStar = alfaStar;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
    	SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		Date date;
		String newDateString = null;
		try {
			date = sdf.parse(createdDate);
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));
			newDateString = sdf2.format(date);
		} catch(ParseException e) {
			e.printStackTrace();
		}
    	
        this.createdDate = newDateString;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
    	SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		Date date;
		String newDateString = null;
		try {
			date = sdf.parse(updatedDate);
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));
			newDateString = sdf2.format(date);
		} catch(ParseException e) {
			e.printStackTrace();
		}
    	
        this.updatedDate = newDateString;
    }
}
