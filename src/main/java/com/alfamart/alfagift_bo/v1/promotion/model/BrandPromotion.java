package com.alfamart.alfagift_bo.v1.promotion.model;

import com.alfamart.alfagift_bo.db.mongo.domain.Brand;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class BrandPromotion implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3204895060428041948L;
	
	private String brandId;
    private String brandName;
    private String imagePath;
    private Integer sortNo;
    private Integer status;
    private Integer createdBy;
    private Integer updatedBy;
    private Integer totalCampaign;
    private String createdDate;
    private String updatedDate;

    public BrandPromotion() {}

    public BrandPromotion(Brand brand) {
        this.brandId = brand.getId();
        this.brandName = brand.getBrandName();
        this.sortNo = brand.getSortNo();
        this.status = brand.getStatus();
        this.createdBy = brand.getCreatedBy();
        this.imagePath = brand.getImagePath();

        if (brand.getUpdatedBy() != null) {
            this.updatedBy = brand.getUpdatedBy();
        }

        this.createdDate = brand.getCreatedAt().toString();

        if (brand.getUpdatedAt() != null) {
            this.updatedDate = brand.getUpdatedAt().toString();
        }

        // todo ask what the fuck is this
        this.totalCampaign = 0;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Integer getTotalCampaign() {
        return totalCampaign;
    }

    public void setTotalCampaign(Integer totalCampaign) {
        this.totalCampaign = totalCampaign;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
    	SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		Date date;
		String newDateString = null;
		try {
			date = sdf.parse(createdDate);
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));
			newDateString = sdf2.format(date);
		} catch(ParseException e) {
			e.printStackTrace();
		}
    	
        this.createdDate = newDateString;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
    	SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		Date date;
		String newDateString = null;
		try {
			date = sdf.parse(updatedDate);
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));
			newDateString = sdf2.format(date);
		} catch(ParseException e) {
			e.printStackTrace();
		}
    	
        this.updatedDate = newDateString;
    }
}
