package com.alfamart.alfagift_bo.v1.base.model;

import io.swagger.annotations.ApiParam;

import javax.ws.rs.HeaderParam;

public class Header {
	
	@HeaderParam("token")
	@ApiParam(value = "Optional, set according to API notes")
	private String token;
	
    @HeaderParam("id")
	@ApiParam(value = "Optional, set according to API notes")
	private String id;
	
	@HeaderParam("trxId")
	@ApiParam(value = "Optional, set according to API notes")
	private String trxId;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
                
        public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}


	
	


}
