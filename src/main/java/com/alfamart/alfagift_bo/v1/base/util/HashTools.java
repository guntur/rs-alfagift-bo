package com.alfamart.alfagift_bo.v1.base.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.apache.commons.codec.digest.DigestUtils;

public class HashTools {
	public static final String SALT = "";
	public static final String CharsetName = "UTF-8";
	public static final Digest DEFAULT_DIGEST = Digest.SHA256;

	public static String toMD5Hex(String value) {
		if (!ValidatorUtil.isNullOrEmpty(value)) {
			return DigestUtils.md5Hex(value.getBytes());
		}
		return "";
	}

	public static String toDigest(String value, Digest algorithm) {
		if (!ValidatorUtil.isNullOrEmpty(value)) {
			try {
				int iterateLength = (algorithm == Digest.SHA1) ? 40 : 32;

				MessageDigest md = MessageDigest.getInstance(algorithm.code());
				md.update(value.getBytes(CharsetName));
				BigInteger hash = new BigInteger(1, md.digest());
				String result = hash.toString(16);
				while (result.length() < iterateLength) { // 40 for SHA-1
					result = "0" + result;
				}
				return result;
			} catch (NoSuchAlgorithmException e) {
			} catch (UnsupportedEncodingException e) {
			}
		}
		return "";
	}

	public static String toSHA256Enhanced(String value) {
		if (!ValidatorUtil.isNullOrEmpty(value)) {
			try {
				MessageDigest md = MessageDigest.getInstance(Digest.SHA256.code());
				byte[] hash = md.digest(value.getBytes(CharsetName));

				StringBuffer result = new StringBuffer();
				for (byte b : hash) {
					result.append(String.format("%02x", b));
				}

				return result.toString();
			} catch (NoSuchAlgorithmException e) {
			} catch (UnsupportedEncodingException e) {
			}
		}
		return "";
	}

	public static String toDigest(String value) {
		return toDigest(value, DEFAULT_DIGEST);
	}

	public static String toMD5HashPHP(String value) {
		return toDigest(value, Digest.MD5);
	}

	public static String toSHA256(String value) {
		return toDigest(value, Digest.SHA256);
	}

	public static String toBase64(String value) {
		return Base64.getEncoder().encode(value.getBytes()).toString();
	}
}
