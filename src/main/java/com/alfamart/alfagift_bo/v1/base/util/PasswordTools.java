package com.alfamart.alfagift_bo.v1.base.util;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.UUID;

public final class PasswordTools {
	 private SecureRandom random = new SecureRandom();

    public String generateRandomString(){
        return new BigInteger(130, random).toString(32);
    }

    public String generateRandomStringByUUID(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    public String generateUniqueRandomPasswordReset(){
        return HashTools.toMD5Hex(generateRandomStringByUUID());
    }

    public String encryptPasswordFromBase64(String value){
        //decrypt base64
        return encryptPasswordFromBase64(value, null);
    }

    public String encryptPasswordFromBase64(String value, String randomString){
        //decrypt base64
        if(!ValidatorUtil.isNullOrEmpty(value)){
            value = new String(Base64.getDecoder().decode(value), Charset.forName(HashTools.CharsetName));
        }
        return encryptPassword(value, randomString);
    }

    public String encryptPassword(String value){
        return encryptPassword(value, null);
    }

    public String encryptPassword(String value, String randomString){
        if(ValidatorUtil.isNullOrEmpty(randomString) || (ValidatorUtil.isNullOrEmpty(randomString) && randomString.length() < 10)){
            randomString = generateRandomString();
        }

        if(!ValidatorUtil.isNullOrEmpty(value)){
            String passAndSalt = randomString + value;
            return HashTools.toSHA256(passAndSalt) + ":" + randomString;
        }

        return null;
    }
	
	public boolean validatePassword(String realPassword, String hashPassword) {
		boolean isValid = false;
		String[] dbPasswordHashArr = hashPassword.split(":");
		String passAndSalt = realPassword;
		if (dbPasswordHashArr.length > 1) {
			passAndSalt = new StringBuilder().append(dbPasswordHashArr[1]).append(passAndSalt).toString();
		}
		try {
			String encryptedPassword = HashTools.toSHA256(passAndSalt);
			int difference = dbPasswordHashArr[0].length() - encryptedPassword.length();
			String password = dbPasswordHashArr[0].substring(difference);

			if (password.equals(encryptedPassword)) {
				isValid = true;
			}
		}catch(StringIndexOutOfBoundsException e){
			isValid = false;
		}
		return isValid;
	}
}
