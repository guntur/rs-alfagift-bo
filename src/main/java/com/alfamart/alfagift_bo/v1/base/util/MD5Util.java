package com.alfamart.alfagift_bo.v1.base.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
	private static final String ALGORITHM = "MD5";
	
	public static String hash(String strHash){
		StringBuffer sb = null;
		try {
			
			MessageDigest md = MessageDigest.getInstance(ALGORITHM);
			md.update((strHash).getBytes());
			
			byte byteData[] = md.digest();
			
		    //convert the byte to hex format method 1
		    sb = new StringBuffer();
		    for (int i = 0; i < byteData.length; i++) {
		     sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		    }
			
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		
		return sb.toString();
	}
	
}
