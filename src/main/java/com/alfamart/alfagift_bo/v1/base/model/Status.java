package com.alfamart.alfagift_bo.v1.base.model;

public class Status
{

	private String code;
	private String message;
	private String trxId;
        private String token;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTrxId() {
		return trxId;
	}
	public void setTrxId(String trxId) {
		this.trxId = trxId;
	}
        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
        


	
	
}