package com.alfamart.alfagift_bo.v1.base.util;

public enum Digest {
	MD5 {
		@Override
		public String code() {
			return "MD5";
		}
	},
	SHA1 {
		@Override
		public String code() {
			return "SHA-1";
		}
	},
	SHA256 {
		@Override
		public String code() {
			return "SHA-256";
		}
	};

	public abstract String code();

	public static Digest getDigestByCode(String code) {
		if (!ValidatorUtil.isNullOrEmpty(code)) {
			for (Digest digest : Digest.values()) {
				if (digest.code().equals(code)) {
					return digest;
				}
			}
		}
		return null;
	}
}
