package com.alfamart.alfagift_bo.v1.base.model;

public class StatusResponse
{
	private Status status;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	
}
