package com.alfamart.alfagift_bo.v1.base.util;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ValidatorUtil {
    public static Boolean isEmptyOrNull(String param) {
        return (param == null) || (param.equals(""));
    }

    public static final String RGX_CONTAINS_NUMBER = ".*[0-u9].*";
    public static final String RGX_CONTAINS_NUMBER_ONLY = "[0-9]*";
    public static final String RGX_CONTAINS_ALPHA_ONLY = "[A-Za-z\\.\\- ]*";
    public static final String RGX_ALPHA_NUMERIC = "[a-zA-z0-9]*";
    public static final String RGX_CONTAINS_SPECIALCHAR = "[a-zA-z0-9\\.\\-]*";
    public static final String RGX_BLACKBERRY = "[a-fA-f0-9]*";

    public static String strNvl(String str, String ifNull) {
        return str == null ? ifNull : str;
    }

    public static String strNvl(Object param, String ifNull) {
        return param != null ? param.toString() : ifNull;
    }

    public static String nvl(Object param) {
        return ValidatorUtil.strNvl(param, "");
    }

    public static String strNvl(String param) {
        return ValidatorUtil.strNvl(param, "");
    }

    public static String strEmpty(String str, String ifEmpty) {
        return "".equals(str) ? ifEmpty : str;
    }

    public static String strNvlOrEmpty(String str, String ifNullorEmpty) {
        return str == null || "".equals(str) ? ifNullorEmpty : str;
    }

    public static boolean isNullOrEmpty(String param) {
        return (param == null) || (param.equals(""));
    }

    public static <E> boolean isNullOrEmpty(List<E> param) {
        return (param == null) || (param.size() == 0);
    }

    public static <E> boolean isNullOrEmpty(ArrayList<E> param) {
        return (param == null) || (param.size() == 0);
    }

    public static boolean isNullOrEmpty(Short temp) {
        return (temp == null);
    }

    public static boolean isNullOrEmpty(Integer param) {
        return param == null || param == 0;
    }

    public static boolean isNullOrEmpty(Double param) {
        return param == null || param == 0;
    }

    public static boolean isNullOrEmpty(Long param) {
        return param == null;
    }

    public static boolean isNullOrEmpty(BigDecimal param) {
        return param == null || param == BigDecimal.ZERO;
    }

    public static boolean isNullOrEmpty(Date date) {
        return date == null;
    }

    public static boolean isNullOrEmpty(Object object) {
        return object == null;
    }

    public static boolean isNullOrEmpty(Object[] object) {
        return object == null || object.length == 0;
    }

    public static boolean containsNumber(String string) {
        return string != null && string.matches(RGX_CONTAINS_NUMBER);
    }

    public static boolean containsNumberOnly(String string) {
        return string != null && string.matches(RGX_CONTAINS_NUMBER_ONLY);
    }

    public static boolean containsAlphaOnly(String string) {
        return string != null && string.matches(RGX_CONTAINS_ALPHA_ONLY);
    }

    public static boolean isAlphaNumeric(String string) {
        return string != null && string.matches(RGX_ALPHA_NUMERIC);
    }

    public static boolean isBlackberryNumeric(String string) {
        return string != null && string.matches(RGX_BLACKBERRY);
    }

    public static boolean isCharAlphaNumeric(String string) {
        return string != null && (string.matches(RGX_CONTAINS_SPECIALCHAR) || string.matches(RGX_ALPHA_NUMERIC));
    }

    public static boolean isEmailAddress(String emailAddress, String mailDomain) {
        return emailAddress != null && emailAddress.matches(".*@" + mailDomain + "$");
    }

    public static boolean checkIfNumber(String in) {
        try {
            new BigDecimal(in);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public static boolean checkIfDouble(String in) {
        try {
            Double.parseDouble(in);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isUrlValid(String url) {
        boolean isValid = true;
        try {
            new URI(url);
        } catch (URISyntaxException e) {
            isValid = false;
        }
        return isValid;
    }

    public static boolean isDateBetween(Date date, Date dateFrom, Date dateTo) {
        return date.after(dateFrom) && date.before(dateTo);
    }

    public static boolean checkExpireDate(Date date) {
        Date recent = new Date();
        int compare = date.compareTo(recent);
        return compare >= 0 ? true : false;
    }

    // public static boolean checkSameDayBook(Date departureDate){
    // try{
    // Date date = new Date();
    // Date dateNow = FormatterUtil.sdf_date.parse(FormatterUtil.sdf_date.format(date));
    // long diff = dateNow.getTime() - departureDate.getTime();
    // long diffDays = diff / (24*60*60*1000);
    //
    // if(diffDays == 0){
    // return true;
    // }
    // }catch(Exception e){
    // return false;
    // }
    // return false;
    // }

    public static boolean isDateOverlaping(Date start1, Date end1, Date start2, Date end2) {
        return (isDateBetween(start1, start2, end2)) || (isDateBetween(end1, start2, end2)) || (start1.compareTo(start2) == 0) || (start1.compareTo(end2) == 0) || (end1.compareTo(start2) == 0) || (end1.compareTo(end2) == 0);
    }

    // public static String checkType(Date dob){
    // try{
    // String type = "Adult";
    // Date date = new Date();
    // Date dateNow = FormatterUtil.sdf_date.parse(FormatterUtil.sdf_date.format(date));
    // long diff = dateNow.getTime() - dob.getTime();
    // long diffDays = diff / (24*60*60*1000);
    //
    // if(diffDays <= 730){
    // type = "Infant";
    // } else if(diffDays > 730 && diffDays <= 4380) {
    // type = "Child";
    // }
    // return type;
    // } catch(Exception e){
    // return null;
    // }
    // }

    // public static String checkTypeCode(Date dob){
    // try{
    // String type = "ADT";
    // Date date = new Date();
    // Date dateNow = FormatterUtil.sdf_date.parse(FormatterUtil.sdf_date.format(date));
    // long diff = dateNow.getTime() - dob.getTime();
    // long diffDays = diff / (24*60*60*1000);
    //
    // if(diffDays <= 730){
    // type = "IN";
    // } else if(diffDays > 730 && diffDays <= 4380) {
    // type = "CH";
    // }
    //
    // return type;
    // } catch(Exception e){
    // return null;
    // }
    // }

    public static String checkGender(String titleCode) {
        String gender = null;

        if (titleCode.equals("Mr."))
            gender = "Male";
        else if (titleCode.equals("Mrs.") || titleCode.equals("Miss"))
            gender = "Female";
        return gender;
    }

    public static String checkName(String name) {

        if (!isNullOrEmpty(name) && name.length() <= 1) {
            name = name + ".";
        }
        return name;
    }

    public static String formatCurrencyIdr(String idr) {
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        String finalIdr = "";
        try {
            formatRp.setCurrencySymbol("Rp ");
            formatRp.setMonetaryDecimalSeparator(',');
            formatRp.setGroupingSeparator('.');

            kursIndonesia.setDecimalFormatSymbols(formatRp);
            // String arrIdr [] = kursIndonesia.format(Long.parseLong(idr)).toString().split(",");
            String arrIdr[] = kursIndonesia.format(Double.parseDouble(idr)).toString().split(",");
            if (arrIdr.length > 0)
                finalIdr = arrIdr[0];
        } catch (Exception e) {
            // TODO: handle exception
        }
        return finalIdr;
    }

    public static String formatCurIdrWithoutRp(String idr) {
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        String finalIdr = "";
        try {
            formatRp.setCurrencySymbol("Rp ");
            formatRp.setMonetaryDecimalSeparator(',');
            formatRp.setGroupingSeparator('.');

            kursIndonesia.setDecimalFormatSymbols(formatRp);
            // String arrIdr [] = kursIndonesia.format(Long.parseLong(idr)).toString().split(",");
            String arrIdr[] = kursIndonesia.format(Double.parseDouble(idr)).toString().split(",");
            if (arrIdr.length > 0)
                finalIdr = arrIdr[0];
        } catch (Exception e) {
            // TODO: handle exception
        }
        return finalIdr;
    }

    public static String randomNumberMandiriClickPay() {
        String randomValue = "";
        String strChars[] = new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        Random randomGenerator = new Random();
        int length = 10;
        for (int i = 0; i < length; i++) {
            randomValue += strChars[randomGenerator.nextInt(strChars.length)];
        }
        return randomValue;
    }

    public static boolean isValidJson(String jsonText) {
        JsonParser parser = new JsonParser();
        boolean isJson = true;
        try {
            JsonObject object = (JsonObject) parser.parse(jsonText);
        } catch (JsonIOException e) {
            try {
                JsonArray object = (JsonArray) parser.parse(jsonText);
            } catch (JsonIOException e1) {
                isJson = false;
            }

        }
        return isJson;
    }

    public static void main(String[] args) {
        try {
            Integer a = 10;
            Integer b = 100;

            if (a.compareTo(b) > 0) {
                System.out.println(a);
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static String newUrl(String url, String id) {
        String newUrl = url.toLowerCase();
        newUrl = newUrl.replaceAll("[^A-Za-z0-9]", " ");
        newUrl = newUrl.trim().replaceAll(" +", " ");
        newUrl = newUrl.replace(" ", "-");
        newUrl += "-" + id;
        return newUrl;
    }

    public static String getId(String url) {
        String[] sUrl = url.split("-");
        String id = sUrl[sUrl.length - 1];
        return id;
    }

    public static String noSpaces(String id) {
        id = id.trim().replaceAll(" +", " ");
        return id;
    }

    public static String getImageUrl(String path) {
        if (path != null && !"".equals(path)) {
            path = Properties.getString("cdn.image.product") + path.substring(17, path.length());
        } else {
            path = "";
        }
        return path;
    }

    public static String nameSubstring(String arg1, int arg2) {
        String str = arg1;
        int lStr = str.trim().length();

        if (lStr > arg2) {
            str = str.substring(0, arg2);
            String[] arr = str.split(" ");
            String strArr = "";
            for (int i = 0; i < arr.length - 1; i++) {
                strArr += arr[i];
                if (i < arr.length - 2)
                    strArr += " ";
            }
            str = strArr + "...";
        }

        return str;
    }

    public static String replaceDiv(String arg1) {
        String strRegEx = "(?s)<[^>]*>(\\s*<[^>]*>)*";
        String str = arg1.replaceAll(strRegEx, " ");
        return str;
    }

    public static double zeroIfNull(BigDecimal number) {
        double hasil = 0;
        if (ValidatorUtil.isNullOrEmpty(number)) {
            hasil = 0;
        } else {
            hasil = number.doubleValue();
        }
        return hasil;
    }
}