package com.alfamart.alfagift_bo.v1.base.model;

/**
 * Created by zer0, the Maverick Hunter
 * on 03/08/18.
 * Class: BaseResponse.java
 */
public class BaseResponse {

    private String message;
    private Integer status;
    private Object data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
