package com.alfamart.alfagift_bo.v1.base.util;

import com.alfamart.alfagift_bo.base.exception.CustomException;
import com.alfamart.alfagift_bo.base.exception.ServiceException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class GenerateUtil {
	public static String getOrderNumber() {
        String datee = new SimpleDateFormat("yyMMdd").format(new Date());
        String returnValue = "O-" + datee + "-AM";
        String strChars[] = new String[] { "B", "C", "D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S",
                "T", "V", "W", "X", "Y", "Z" };
        Random randomGenerator = new Random();
        int length = 5;
        for (int i = 0; i < length; i++) {
            returnValue += strChars[randomGenerator.nextInt(strChars.length)];
        }
        return returnValue;
    }
    
    public static String getDeliveryNumber() {
        String datee = new SimpleDateFormat("yyMMdd").format(new Date());
        String returnValue = "S-" + datee + "-AM";
        String strChars[] = new String[] { "B", "C", "D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S",
                "T", "V", "W", "X", "Y", "Z" };
        Random randomGenerator = new Random();
        int length = 5;
        for (int i = 0; i < length; i++) {
            returnValue += strChars[randomGenerator.nextInt(strChars.length)];
        }
        return returnValue;
    }

    public static Date convertTimestampUnixToDate(long timestampunix){
        //long unixSeconds = 1506407512;
        Date date = new Date ();
        date.setTime((long)timestampunix*1000);
        System.out.println(date);


        return date;
    }
    
    public static XMLGregorianCalendar convertTimestampToGeorgianCalendar(long timestampunix) throws DatatypeConfigurationException{
        //long unixSeconds = 1506407512;
    	//long time = 1379487623;
    	Date date = new Date((long)timestampunix*1000);
    	GregorianCalendar c = new GregorianCalendar();
    	c.setTime(date);
    	XMLGregorianCalendar dateXmlGeorgian;
		
		dateXmlGeorgian = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		
        return dateXmlGeorgian;
    }

    public static String constructOtpMessage(Long otpCode, String action, Date validUntil) throws ServiceException, CustomException {

        String beforeFormat = "EEE MMM dd HH:mm:ss Z yyyy";
        String afterFormat = "dd-MM-yyyy HH:mm:ss";

        String msg = Properties.getString("alfagift.otp.message");

        msg = msg.replace("{action}", action);
        msg = msg.replace("{otpcode}", otpCode.toString());
        try {
            msg = msg.replace("{valid}", changeDateFormat(validUntil.toString(), beforeFormat, afterFormat));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return msg;
    }

    public static Date generateTotp() throws ServiceException, CustomException {

        Calendar now = Calendar.getInstance();

        String otpValidUntil = Properties.getString("alfagift.otp.ttl");

        try {

            String[] splittedOtpValidUntil = otpValidUntil.split("-");

            if (splittedOtpValidUntil.length != 2) {
                throw new ServiceException(Constant.ResponseCode.EXCEPTION, Constant.ResponseMessage.OTP_TTL_PROPERTIES_INVALID);
            }

            Integer parsedNumInt = Integer.valueOf(splittedOtpValidUntil[0]);

            switch (splittedOtpValidUntil[1]) {
                case "s" :
                    now.add(Calendar.SECOND, parsedNumInt);
                    break;
                case "m" :
                    now.add(Calendar.MINUTE, parsedNumInt);
                    break;
                case "h" :
                    now.add(Calendar.HOUR, parsedNumInt);
                    break;
                case "d" :
                    now.add(Calendar.DATE, parsedNumInt);
                    break;
            }
        } catch (ServiceException e) {
            throw new CustomException(Constant.ResponseMessage.OTP_TTL_PROPERTIES_INVALID);
        }

        return now.getTime();
    }

    public static String changeDateFormat(String value, String beforeFormat, String afterFormat) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(beforeFormat);
        Date cal = sdf.parse(value);
        SimpleDateFormat sdf1 = new SimpleDateFormat(afterFormat);
        return sdf1.format(cal);
    }
}