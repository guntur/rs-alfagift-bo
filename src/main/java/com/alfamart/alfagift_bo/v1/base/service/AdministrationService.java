/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.v1.base.service;

import com.alfamart.alfagift_bo.db.postgresql.Manager;
import com.alfamart.alfagift_bo.db.postgresql.TbSystemNotificationManager;
import com.alfamart.alfagift_bo.v1.base.util.ClassUtil;
import com.alfamart.alfagift_bo.db.exception.DAOException;
import com.alfamart.alfagift_bo.db.exception.DataAccessException;
import com.alfamart.alfagift_bo.db.postgresql.TbSystemNotificationBean;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;
import java.sql.SQLException;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author Elko
 */
@Service
public class AdministrationService {
    public Logger log = LoggerFactory.getLogger(this.getClass());
    public boolean submitNotification(Integer tbsncId, Integer tbsntId, Integer tbmmId, String orderNo,
			String subject, String datas[]) throws DAOException {
		boolean commit = false;

		try {
			Manager.getInstance().beginTransaction();
			String strData = "";

			for (int i = 0; i < datas.length; i++) {
				strData += datas[i];
				if (i != datas.length - 1)
					strData += "[|]";
			}

			TbSystemNotificationBean tbSystemNotificationBean = TbSystemNotificationManager.getInstance()
					.createTbSystemNotificationBean();
			tbSystemNotificationBean.setTbsntCreateDate(new Date());
			tbSystemNotificationBean.setTbsntCreateId(0);
			tbSystemNotificationBean.setTbmmId(tbmmId);
			tbSystemNotificationBean.setTbsncId(tbsncId);
			tbSystemNotificationBean.setTbsntId(tbsntId);
			tbSystemNotificationBean.setTbsnSubject(ValidatorUtil.strNvl(subject, "-"));
			tbSystemNotificationBean.setTbsnData(strData);
			tbSystemNotificationBean.setTbsnStatus(81);
			if (!ValidatorUtil.isNullOrEmpty(orderNo)) {
				tbSystemNotificationBean.setTbtoId(Integer.parseInt(orderNo));
			}
			TbSystemNotificationManager.getInstance().save(tbSystemNotificationBean);

			commit = true;
		} catch (SQLException e) {
			ClassUtil.getInstance().logError(log, e);
			throw new DataAccessException(e);
		} finally {
			try {
				Manager.getInstance().endTransaction(commit);
			} catch (SQLException e) {
				ClassUtil.getInstance().logError(log, e);
			}
		}

		return commit;
	}
}
