package com.alfamart.alfagift_bo.v1.base.common;

/**
 * Modified by zer0, the Maverick Hunter
 * on 24/07/18.
 * */
public class RestUrl {
	
	public class V1 {
		// PROMOTION SERVICE
		public static final String PROMOTION_ROOT = "v1/promotion/";
		public static final String BRAND_GET = "brand/get";
		public static final String BRAND_SAVE = "brand/saveUpdate";

		// INTEREST SERVICE
		public static final String INTEREST_ROOT = "v1/interest/";
		public static final String INTEREST_GET = "get";

		// OTP SERVICE
		public static final String OTP_ROOT = "v1/otp/";
		public static final String OTP_REQUEST = "request";
		public static final String OTP_VERIFY = "verify";

		// ACCOUNT SERVICE
		public static final String ACCOUNT_ROOT = "v1/account";
		public static final String ACCOUNT_PROVINCE_GET = "province/get";
		public static final String ACCOUNT_CITY_GET= "city/get";
		public static final String ACCOUNT_DISTRICT_GET= "district/get";
		public static final String ACCOUNT_SUB_DISTRICT_GET= "subDistrict/get";

		// MEMBER SERVICE
		public static final String ACCOUNT_CHECK= "member/check";
		public static final String ACCOUNT_MEMBER= "member/get";
		public static final String ACCOUNT_REGISTER_MEMBER= "member/register";
		public static final String ACCOUNT_REGISTER_MEMBER2= "member/register2";
		public static final String ACCOUNT_UPDATE_MEMBER= "member/edit";
		public static final String ACCOUNT_MEMBER_GET_ADDRESS= "address/get";
		public static final String ACCOUNT_MEMBER_EDIT_ADDRESS= "address/edit";
		public static final String ACCOUNT_MEMBER_LOGIN= "member/login";
		public static final String ACCOUNT_CREATE_MEMBER_GROUP= "member/group/add";
		public static final String ACCOUNT_CHANGE_PASSWORD= "member/changePassword";
		public static final String ACCOUNT_FORGOT_PASSWORD= "member/forgotPassword";
		public static final String ACCOUNT_CREATE_DUMMY= "member/dummy";
		public static final String ACCOUNT_MEMBER_INTEREST= "member/interest";

		// PRODUCT SERVICE
		public static final String PRODUCT_ROOT = "v1/product";
		public static final String PRODUCT_GET = "v1/product/get";
		public static final String PRODUCT_VIRTUAL_GET = "v1/product/virtual/get";

		// file service
		public static final String UPLOAD_IMAGES = "/upload/images/{card}/{memberId}";
		public static final String SHOW_IMAGES = "/show/images/{image}";

	}
}
