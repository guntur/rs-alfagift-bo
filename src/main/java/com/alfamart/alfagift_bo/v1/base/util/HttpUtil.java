package com.alfamart.alfagift_bo.v1.base.util;


import com.alfamart.alfagift_bo.v1.base.model.Header;
import okhttp3.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;


public class HttpUtil {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private static OkHttpClient client = new OkHttpClient();
	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

	private static HttpUtil singleton = new HttpUtil();

	public static HttpUtil getInstance() {
		return singleton;
	}
	

	public String sendHttpPostResponseBody(String url, Integer timeOut, String params) throws ClientProtocolException, IOException {
		log.info("url :"+url+" **params :"+params);
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost post = new HttpPost(url);
		post.setHeader("Content-Type", "application/json;charset=utf-8");
		HttpEntity entity = new ByteArrayEntity(params.getBytes("UTF-8"));
		post.setEntity(entity);
		HttpResponse response = httpclient.execute(post);
		log.info("response :"+response);
		String result = EntityUtils.toString(response.getEntity());
		log.info("result :"+result);	
		log.info("********************Http Response Back End************************");		
		return result.toString();
	}

	public String sendHttpGetRequest(String fulUri, Header header) throws IOException, URISyntaxException {

		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet get = new HttpGet(fulUri);

		if (!ValidatorUtil.isNullOrEmpty(header)) {
			get.setHeader("token", header.getToken());
			get.setHeader("trxId", header.getTrxId());
		}

		HttpResponse response = httpclient.execute(get);
		log.info("response :"+response);
		String result = EntityUtils.toString(response.getEntity());
		log.info("result :"+result);
		log.info("********************Http Response Back End************************");
		return result.toString();
	}

	public boolean curlURL(String url) throws SocketTimeoutException, SSLHandshakeException {
		URL target = null;
		try {
			target = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) target.openConnection();
			connection.setRequestMethod("HEAD");
			connection.setConnectTimeout(8000);
			connection.setReadTimeout(8000);
			connection.getInputStream();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			throw new SocketTimeoutException("Error accessing stream URL !");
		}
		return true;
	}

	public static String get(String url, Map<String, String> headerMap) throws IOException {
		if (headerMap == null) {
			headerMap = new HashMap<String, String>();
		}
		headerMap.put("content-type", "application/json");
		Headers headers = Headers.of(headerMap);
		Request request = new Request.Builder().url(url).get().headers(headers).build();

		try (Response response = client.newCall(request).execute()) {
			return response.body().string();
		}
	}

	public static String post(String url, String postData, Map<String, String> headerMap) throws IOException {

		RequestBody body = RequestBody.create(JSON, postData);
		if (headerMap == null) {
			headerMap = new HashMap<String, String>();
		}
		headerMap.put("content-type", "application/json");
		Headers headers = Headers.of(headerMap);
		Request request = new Request.Builder().url(url).post(body).headers(headers).build();

		try (Response response = client.newCall(request).execute()) {
			return response.body().string();
		}

	}
}
