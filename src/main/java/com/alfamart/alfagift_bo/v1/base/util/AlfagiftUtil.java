package com.alfamart.alfagift_bo.v1.base.util;

import com.alfamart.alfagift_bo.v1.base.util.ClassUtil;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zer0, the Maverick Hunter
 * on 27/07/18.
 * Class: AlfagiftUtil.java
 */
public class AlfagiftUtil {

    private Logger log = LoggerFactory.getLogger("bov2");

    /**
     * Method for upload image and save at main banner management
     *
     * @author akbar.negara
     * @return
     * @throws IOException
     * @throws ServletException
     */
    public String uploadImage(String prefix, MultipartFile mf, String path) throws IOException, ServletException {
        String fileName = null;
        String result = null;
        String[] extension = null;

        if (!mf.isEmpty()) {
            try {
                //set imageName
                extension = mf.getOriginalFilename().split("\\.");
                DateFormat df = new SimpleDateFormat("yyMMdd");
                Date today = new Date();
                String todayDate = df.format(today);

                fileName = prefix + todayDate + "_" + generateRandomString() + "." + extension[extension.length-1];
                byte[] bytes = mf.getBytes();

                //save image
                BufferedOutputStream buffStream = new BufferedOutputStream(new FileOutputStream(new File(path + "/" + fileName)));
                buffStream.write(bytes);
                buffStream.close();
                result =  path + "/" + fileName;
                Runtime.getRuntime().exec("chmod 777 " + result);
            } catch (Exception e) {
                ClassUtil.getInstance().logError(log, e);
            }
        }
        return result;
    }

    /**
     * Method for generate 4 digit random alphanumeric
     *
     * @author akbar.negara
     * @return
     */
    public String generateRandomString(){
        String shortId = RandomStringUtils.randomAlphanumeric(4);
        return shortId;
    }

}
