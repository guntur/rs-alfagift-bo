package com.alfamart.alfagift_bo.v1.base.util;

public final class Constant {

    public static final class ResponseCode {
        public static final String SUCCESS = "00";
        public static final String FAILED = "01";
        public static final String TOKEN_FAILED = "02";
        public static final String TRX_ID_FAILED = "03";
        public static final String DUPLICATE_ENTRY = "04";
        public static final String FORBIDDEN = "05";
        public static final String OTP_EXPIRED = "06";
        public static final String BAD_REQUEST = "07";
        //
        public static final String EXCEPTION = "10";
        public static final String DAO_EXCEPTION = "11";
        //
        public static final String EMPTY_RESULT = "99";
        public static final String WRONG_TOKEN = "98";
        public static final String DATA_EMPTY = "101";
    }

    public static final class ResponseMessage {
        public static final String SUCCESS = "Success";
        public static final String FAILED = "Failed";
        public static final String TOKEN_FAILED = "Token Failed";
        public static final String TRX_ID_FAILED = "Trx Id Failed";
        public static final String DATA_EMPTY = "Token Empty";
        public static final String FORBIDDEN = "Action not allowed";
        public static final String BAD_REQUEST = "Bad request";
        public static final String PONTA_FAILED = "Failed to communicating with Ponta Service";
        public static final String PONTA_TOKEN_FAILED = "Failed setup Ponta Token";
        public static final String GLOBAL_ERROR = "We're sorry but something went wrong";
        public static final String EMPTY_RESULT = "Empty Result";

        // MODULE SPECIFIC MESSAGE
        // OTP
        public static final String OTP_SENT = "OTP Sent";
        public static final String OTP_PHONE_NUMBER_EMPTY = "Phone Number Cannot Empty";
        public static final String OTP_ACTION_EMPTY = "OTP Action Cannot Empty";
        public static final String OTP_ACTION_NOT_RECOGNIZED = "OTP Action is not allowed";
        public static final String OTP_TTL_PROPERTIES_INVALID = "TTL Properties is invalid or not configured properly, hint : {alfagift.otp.ttl}";
        public static final String OTP_GATEWAY_INVALID = "OTP Gateway Properties is invalid or not configured properly, hint : {alfagift.otp.gateway}";
        public static final String OTP_SEND_FAILED = "Something went wrong when contacting OTP Gateway, please try again in a moment";
        public static final String OTP_VERIFY_FAILED = "Invalid OTP code";
        public static final String OTP_EXPIRED = "Your OTP has expired, please request a new one";
        public static final String OTP_DESC_EXPIRED = "OTP Expired";

        // MEMBER
        public static final String MEMBER_LOGIN_PHONE_NOT_REGISTERED = "Please register first";
        public static final String MEMBER_LOGIN_UPDATE_PROFILE_FIRST = "Please update your profile";
        public static final String MEMBER_LOGIN_INPUT_PASSWORD = "Please input your password";
        public static final String MEMBER_LOGIN_PASS_INVALID = "Password invalid";
        public static final String MEMBER_EMAIL_DUPLICATE = "Email already registered";
        public static final String MEMBER_EMAIL_INVALID = "Invalid email format";
        public static final String MEMBER_PHONE_DUPLICATE = "Phone already registered";
        public static final String MEMBER_PHONE_INVALID = "Invalid Phone number format";
        public static final String MEMBER_LOGIN_PASS_EMPTY = "Password cannot empty";
        public static final String MEMBER_NOTFOUND = "User not found";
        public static final String MEMBER_REGISTER_SUCCESS = "Successfully regsiter user";
        public static final String MEMBER_REGISTER_FAILED = "Registering user failed";
        public static final String MEMBER_AVATAR_FAILED = "Upload member avatar failed";
        public static final String MEMBER_FORGOT_PASSWD_EMAIL_NOTFOUND = "Email is not registered";
        public static final String MEMBER_FORGOT_PASSWD_PHONE_NOTFOUND = "Phone is not registered";
        public static final String MEMBER_FORGOT_PASSWD_INVALID_INPUT = "Invalid phone/email format";
        public static final String MEMBER_PASSWORD_RESET_SUCCESS = "Your password has been changed";
        public static final String MEMBER_INTEREST_SAVE_NULL = "Getting Null Interest Id, service nothing";
        public static final String MEMBER_INTEREST_SAVE_FAILED = "Error when saving Member Interests";
        public static final String MEMBER_GET_FAILED = "Error when saving Getting Member Data";
    }

    public static final class PontaErrorCode {
        public static final String SESSION_EXPIRED = "E004";
        public static final String ERR_SQL_QUERY_ERR = "E007";
        public static final String UNKNOWN_DATA_TYPE = "E009";
        public static final String CARD_NOT_EXISTS = "E025";
        public static final String CUSTOMER_ALREADY_ENROLLED = "E050";
    }

    public static final class PontaErrorMessage {
        public static final String SESSION_EXPIRED = "Ponta Session Expired";
        public static final String ERR_SQL_QUERY_ERR = "Ponta SQL query error";
        public static final String UNKNOWN_DATA_TYPE = "Unknown data type";
        public static final String CARD_NOT_EXISTS = "Card did not exists";
        public static final String CUSTOMER_ALREADY_ENROLLED = "Customer Already Enrolled";
    }

    public static final class PromotionModule {
        public static final Integer ACTIVE = 63;
        public static final Integer INACTIVE = 64;
        public static final Integer PROMO_DISABLE = 24;
        public static final Integer PROMO_ENABLE = 23;
        public static final Integer SPECIAL_PRICE_PROMO_TYPE = 178;
        public static final Integer PAYMENT_PROMO_TYPE = 175;
        public static final Integer SHOPPING_CART_PROMO_TYPE = 174;
        public static final Integer BULKY_PROMO_TYPE = 000;
        public static final Integer SHIPMENT_PROMO_TYPE = 173;
        public static final Integer VOUCHER_ISSUED_APPROVED = 31;
        public static final Integer VOUCHER_ISSUED_CANCELED = 39;
        public static final Integer VOUCHER_GEN_TYPE_NOCODE = 158;
        public static final Integer VOUCHER_GEN_TYPE_CODE = 157;
    }

    public static final class errorMessage {

        public static final String SUCCESS = "SUCCESS";
        public static final String VA_MODEL_EMPTY = "VA_MODEL_EMPTY";
        public static final String EMPTY = "EMPTY DATA";
        public static final String GENERAL_ERROR = "GENERAL ERROR";
        public static final String NON_PENDING = "YOUR TRANSACTION HAS BEEN PAID";
        public static final String GRAND_TOTAL = "AMOUNT NOT MATCH";
        public static final String INSERT_ERROR = "FAILED TO INSERT TO DATABASE";
        public static final String ONE_AT_A_TIME = "CANNOT PROCESS INQUIRY AND PAYMENT AT A TIME";

        public static final String PAYMENT_PROCESSING_FAIL = "PAYMENT_PROCESSING_FAIL";
        public static final String MAPPING_ORDER_FAIL = "MAPPING_ORDER_FAIL";
        public static final String INVALID_DENOMINATION = "INVALID_DENOMINATION";
        // Akbar penambahan error untuk Doku: 23-01-2017
        public static final String PIN_TOO_LONG = "Terjadi kesalahan pengetikan kode CVV";
        public static final String PIN_NOT_VALID_FORMAT = "Format CVV salah";
        public static final String SESSIONID_NOT_VALID = "Email yang Anda masukkan salah (cek karakter yang diinput)";
        public static final String SO_NOT_FOUND = "Store Owner tidak ditemukan";
        public static final String PIN_NOT_VALID = "CVV yang Anda masukkan salah";
        public static final String SO_NOT_ACTIVE = "Store Owner tidak aktif";
        public static final String UNEXPECTED_ERROR = "Terjadi kendala jaringan";
        public static final String FAILED_BO_ADD_PRODUCT = "FAILED ADD PRODUCT";
        public static final String SUCCESS_BO_ADD_PRODUCT = "SUCCESS ADD PRODUCT";
        public static final String INVALID_PARAMETER = "INVALID PARAMETER";
        public static final String TRANSACTION_EXPIRED = "TRANSCTION EXPIRED";
        public static final String TRANSACTION_CANCELED = "TRANSACTION CANCELED";
        public static final String TRANSACTION_DECLINE = "TRANSACTION DECLINE";
        public static final String INCORRECT_VIRTUAL_ACCOUNT = "INCORRECT VIRTUAL ACCOUNT";
    }

    public static final class DeliveryConstantVariable {
        public static final Integer PICKUP = 73;
        public static final Integer DELIVERY = 74;
        public static final Integer PICKUP_PREPERATION_TIME = 4;
        public static final Integer DELIVERY_PREPERATION_TIME = 6;
        public static final String COUNTRY_ID = "ID";
    }

    // Status For FO
    public static class CustomerRegisterStatus {
        public static final String REGISTER_UPDATE_SUCCESS = "00";
        public static final String REGISTER_UPDATE_FAILED = "11";
        public static final String EMAIL_ALREADY_EXIST = "12";
        public static final String NEWSLETTER_TRUE = "T";
        public static final String NEWSLETTER_FALSE = "F";
    }

    public static final class CustomerRegisterMessage {
        public static final String REGISTER_UPDATE_SUCCESS = "REGISTER OR UPDATE SUCCESS";
        public static final String REGISTER_UPDATE_FAILED = "REGISTER OR UPDATE FAILED";
        public static final String EMAIL_ALREADY_EXIST = "EMAIL ALREADY EXIST";
        public static final String EMAIL_EXIST_MSG = "Email already exists";
    }

    public static final class Token {
        public static final String APPLICATION_SECRET = "@lf4m1nd5s3cret2018";
        public static final String CURRENT_TOKEN = "CURRENT_TOKEN";
        public static final String INVALID_TOKEN_MSG = "TOKEN INVALID";
        public static final String VALID_TOKEN_MSG = "TOKEN VALID";
        public static final String INVALID_WHITELIST = "INVALID WHITELIST";
        public static final String VALID_TOKEN_STATUS = "00";
        public static final String INVALID_TOKEN_STATUS = "11";
        public static final String WS_TOKEN = "alfacart";
    }

    public static final class CustomerLoginStatus {
        public static final String LOGIN_SOSMED_TRUE = "T";
        public static final String LOGIN_SOSMED_FALSE = "F";
        public static final String LOGIN_MANUAL = "M";
        public static final String LOGIN_SOSMED = "S";
    }

    public static final class CustomerLoginMessage {
        public static final String INVALID_ERROR_MSG = "Invalid username or password";
        public static final String INVALID_EMAIL_MSG = "Email not exists";
    }

    public static final class CustomerForgotPass {
        public static final String FAILED = "Reset Password Failed";
        public static final String EXPIRED = "Reset Password has expired";
    }

    public static final class LoginSocialMedia {
        public static final String GOOGLE_CLIENT_ID = "663734978856-2nqnph9irkb442p89qb8fu9qn0i1fv9g.apps.googleusercontent.com";
        public static final String GOOGLE_CLIENT_SECRET = "99yGw8XCPWRC0tWLDJtxJ8_z";
        public static final String GOOGLE_REDIRECT_URL = "http://localhost:8080/fo/graphGooglePlus";
    }

    public static final class NotificationChanel {
        public static final int EMAIL = 1;
        public static final int SMS = 2;
    }

    public static final class NotificationTemplate {
        public static final int CUSTOMER_REGISTER = 28;
        public static final int CUSTOMER_FORGOTPASS = 29;
        public static final int SHOPPINGLIST_TO_FRIEND = 38;
    }

    public static final class PostgreTransactionStatus {
        public static final int PENDING = 46;
        public static final int PAID = 47;
        public static final int CANCEL = 48;
        public static final int FAILED = 79;
    }

    public static final class PotgreOrderStatus {
        public static final int PENDING = 11;
        public static final int PAID = 12;
        public static final int CANCEL = 18;
    }

    public static final class NotificationSubject {
        public static final String CUSTOMER_REGISTER = "Selamat Datang";
        public static final String CUSTOMER_FORGOTPASS = "Forgot Password";
        public static final String SHOPPINGLIST_TO_FRIEND = "Shoppinglist";
    }

    public static final class CustomerAddressBookStatus {
        public static final String SUCCESS_GET_ADDRESS_BOOK = "01";
        public static final String FAILED_GET_ADDRESS_BOOK = "00";
        public static final String SUCCESS_SAVE_ADDRESS_BOOK = "11";
        public static final String FAILED_SAVE_ADDRESS_BOOK = "10";
    }

    public static final class CustomerAddressBookMessage {
        public static final String SUCCESS_GET_ADDRESS_BOOK = "Success to get address book";
        public static final String FAILED_GET_ADDRESS_BOOK = "Failed to get address book";
        public static final String SUCCESS_SAVE_ADDRESS_BOOK = "Alamat berhasil tersimpan";
        public static final String FAILED_SAVE_ADDRESS_BOOK = "Alamat tidak berhasil tersimpan";
        public static final String SUCCESS_DELETE_ADDRESS_BOOK = "Alamat berhasil dihapus";
        public static final String FAILED_DELETE_ADDRESS_BOOK = "Alamat tidak berhasil dihapus";
        public static final String SUCCESS_UPDATE_ADDRESS_BOOK = "Alamat berhasil diubah";
        public static final String FAILED_UPDATE_ADDRESS_BOOK = "Alamat tidak berhasil diubah";
    }

    public static final class CustomerInfoStatus {
        public static final String SUCCESS_GET_INFO = "01";
        public static final String FAILED_GET_INFO = "00";
        public static final String SUCCESS_SAVE_INFO = "11";
        public static final String FAILED_SAVE_INFO = "10";
    }

    public static final class CustomerInfoMessage {
        public static final String SUCCESS_GET_INFO = "Success to get customer info";
        public static final String FAILED_GET_INFO = "Failed to get customer info";
        public static final String SUCCESS_SAVE_INFO = "Perubahan Informasi Akun berhasil";
        public static final String FAILED_SAVE_INFO = "Failed to save customer info";
        public static final String SUCCESS_TITTLE = "Sukses";
        public static final String FAILED_TITTLE = "Gagal";
    }

    public static final class CustomerPasswordStatus {
        public static final String SUCCESS_CHANGE_PASSWORD = "01";
        public static final String FAILED_CHANGE_PASSWORD = "00";
    }

    public static final class CustomerPasswordMessage {
        public static final String SUCCESS_CHANGE_PASSWORD = "Perubahan Password Akun berhasil";
        public static final String FAILED_CHANGE_PASSWORD = "Perubahan Password Akun tidak berhasil";
    }

    public static final class CustomerSummaryStatus {
        public static final String SUCCESS_GET_SUMMARY = "01";
        public static final String FAILED_GET_SUMMARY = "00";
        public static final String BILLING_ADDRESS_TRUE = "T";
        public static final String BILLING_ADDRESS_FALSE = "F";
        public static final String DELIVERY_ADDRESS_TRUE = "T";
        public static final String DELIVERY_ADDRESS_FALSE = "F";
        public static final String SUBSCRIBE_NEWSLETTER_TRUE = "T";
        public static final String SUBSCRIBE_NEWSLETTER_FALSE = "F";
    }

    public static final class CustomerSummaryMessage {
        public static final String SUCCESS_GET_SUMMARY = "Success to get customer summary";
        public static final String FAILED_GET_SUMMARY = "Failed to get customer summary";
    }

    public static final class CustomerSummaryOrderStatus {
        public static final String[] UNPAID = { "11" };
        public static final String[] PROCESS = { "12", "14" };
        public static final String[] COMPLETE = { "15" };
        public static final String[] CANCEL = { "18", "105" };
    }

    public static final class Month {
        public static final String[] ID = { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" };
        public static final String[] EN = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
    }

    public static final class AreaStatus {
        public static final String SUCCESS_GET_AREA = "01";
        public static final String FAILED_GET_AREA = "00";
    }

    public static final class AreaMessage {
        public static final String SUCCESS_GET_AREA = "Success to get area";
        public static final String FAILED_GET_AREA = "Failed to get area";
    }

    public static final class StorageService {
        public static final String UPLOAD_DIR = "/assets/desktop/img/profile-pic/";
        public static final String UPLOAD_DIR_SYSTEM = "C:\\home\\images\\fo\\profile-pic\\";
        public static final String UPLOAD_PATH = "images/fo/profile-pic/";
    }

    public static final class EmailSubscribeStatus {
        public static final String SUCCESS = "00";
        public static final String FAILURE = "01";
        public static final String ERROR = "02";
    }

    public static final class EmailSubscribeMessage {
        public static final String SUCCESS = "Berhasil berlangganan Newsletter";
        public static final String FAILURE = "Gagal berlangganan, Email tidak ditemukan";
        public static final String ERROR = "Gagal berlangganan, silahkan coba lagi";
    }

    public static final class OrderStatus {
        public static final String PENDING_PAYMENT = "11";
        public static final String PAID = "12";
        public static final String ACCEPTED = "13";
        public static final String ON_DELIVERY = "14";
        public static final String DELIVERY_COMPLETED = "15";
        public static final String CANCEL_COMPLETED = "18";
        public static final String CANCEL_BY_ADMIN = "105";
    }

    public static final class EventNotification {

        public static final Integer SEND_NOFICATION_ID = 1;
        public static final Integer PRODUCT_READY_STOCK_ID = 2;
        public static final Integer STATUS_ON_PROGRESS = 81;
        public static final Integer STATUS_DONE = 82;
        public static final Integer PRODUCT_READY_STOCK_TEMPLATE_ID = 39;
        public static final Integer SYSTEM_MEMBER_ID = 0;
        public static final Integer SHOPPING_LIST_NOTIFICATION_TEMPLETE_ID = 40;
        public static final String SHOPPING_LIST_URL = "https://www.alfacart.com/shoppinglist/publicview/";
    }

    public static final class DeliveryB2B {
    	
    	public static final String READY_TO_PICKUP = "READY_TO_PICKUP";
    	public static final String ON_DELIVERY = "ON_DELIVERY";
    	public static final String DELIVERY_COMPLETED = "DELIVERY_COMPLETED";
    	
    	public static final int READY_TO_PICKUP_STATUS = 107;
    	public static final int ON_DELIVERY_STATUS  = 14;
    	public static final int DELIVERY_COMPLETED_STATUS = 15;

    }

    public static final class DateFormat {
        public static final String SYS_DEFAULT = "EEE MMM dd HH:mm:ss Z yyyy";
        public static final String MONGO = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    }

}
