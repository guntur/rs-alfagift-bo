/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.v1.base.service;

import com.alfamart.alfagift_bo.base.exception.ServiceException;
import com.alfamart.alfagift_bo.v1.base.util.ClassUtil;
import com.alfamart.alfagift_bo.v1.base.util.Constant;
import com.alfamart.alfagift_bo.v1.base.util.Properties;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import javax.activation.DataHandler;
import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author Elko
 */
@Service("fileUploadService")
public class FileUploadServiceImpl implements FileUploadService{

    public org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());
    String reponsePath = "";
    private String getPath()   {
        try{
            String path = this.getClass().getClassLoader().getResource("").getPath();
            String fullPath = URLDecoder.decode(path, "UTF-8");
            String pathArr[] = fullPath.split("/target/rs-alfamind/WEB-INF/classes/");
            System.out.println("full path : "+fullPath);
            System.out.println("full path : "+pathArr[0]);
            fullPath = pathArr[0];

            // to read a file from webcontent
//            reponsePath = new File(Properties.getString("customer.upload.dir.win")).getPath() ;
            reponsePath = new File(Properties.getString("customer.upload.dir")).getPath() ;
        }catch(UnsupportedEncodingException ex){logger.error(ex.toString());}
        System.out.println("property : "+Properties.getString("customer.upload.dir"));
        System.out.println("property : "+Properties.getString("customer.upload.dir.win"));
       
        return reponsePath;
       // return Properties.getString("customer.upload.dir.win");
    }
    
           
    /**
     * Creates a folder to desired location if it not already exists
     * 
     * @param dirName
     *            - full path to the folder
     * @throws SecurityException
     *             - in case you don't have permission to create the folder
     */
    private void createFolderIfNotExists(String dirName)
                    throws SecurityException {
            File theDir = new File(dirName);
            if (!theDir.exists()) {
                    theDir.mkdir();
            }
    }
    
    public String clientUpload(String dirFile,String host){
        // the file we want to upload
		File inFile = new File(dirFile);
		FileInputStream fis = null;
                boolean result = false;
                String responseString =null;
		try {
			fis = new FileInputStream(inFile);
                        
			DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
			
			// server back-end URL
			HttpPost httppost = new HttpPost(host+"upload/images");
			MultipartEntity entity = new MultipartEntity();
			// set the file input stream and file name as arguments
			entity.addPart("file", new InputStreamBody(fis, inFile.getName()));
			httppost.setEntity(entity);
			// execute the request
			HttpResponse response = httpclient.execute(httppost);
			
			int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity responseEntity = (HttpEntity) response.getEntity();
			responseString = EntityUtils.toString(responseEntity,"UTF-8");
			
			System.out.println("[" + statusCode + "] " + responseString);
			result = true;
		} catch (ClientProtocolException e) {
			System.err.println("Unable to make connection");
		} catch (IOException e) {
			System.err.println("Unable to read file");
		} finally {
			try {
				if (fis != null) fis.close();
			} catch (IOException e) {}

                return responseString;
                }

    }
    
      public static void main(String args[]) {
          FileUploadServiceImpl impl =new FileUploadServiceImpl();
          System.out.println("properties : "+Properties.getString("customer.upload.dir"));
          System.out.println("berhasil upload ktp: "+impl.clientUpload("C:\\Users\\Elko\\Downloads\\sample1.png","http://localhost:8080/alfamind/"));
          System.out.println("berhasil upload npwp: "+impl.clientUpload("C:\\Users\\Elko\\Downloads\\sample1.png","http://localhost:8080/alfamind/"));
          System.out.println("long : "+System.currentTimeMillis());
      }
    
      
    @Override
      public Response downloadImageFile() {
 
        File file = null;
       
            file = new File(getPath()+"\\images\\"+"\\1516705331786.PNG");
        
 
        ResponseBuilder responseBuilder = Response.ok((Object) file);
        responseBuilder.header("Content-Disposition", "attachment; filename=\"MyImageFile.png\"");
        return responseBuilder.build();
    }
 
    @Override
    public Response uploadImageFile(List<Attachment> attachments,  UriInfo ui,String card,String memberId) {
 
        // local variables
        DataHandler dataHandler = null;
        MultivaluedMap<String, String> multivaluedMap = null;
        String fileName = null;
        InputStream inputStream = null;
        String UPLOAD_FOLDER = null;
        String fn =null;
        
                  // check if all form parameters are provided
                  UPLOAD_FOLDER =getPath()+"/images/"+card;
         System.out.println("-------------- upload folder : "+UPLOAD_FOLDER);     
        for (Attachment attachment : attachments) {
            dataHandler = attachment.getDataHandler();
            try{
                // get filename to be uploaded
                multivaluedMap = attachment.getHeaders();
                fileName = getFileName(multivaluedMap);
                System.out.println("file name : "+fileName);
                String extArr[] =fileName.split("\\.");
                System.out.println("ext : "+extArr[1]);
                System.out.println("URL contex : "+ui.getBaseUri());
                System.out.println("URL contex 2: "+ui.getAbsolutePath());
 
                try {
			createFolderIfNotExists(UPLOAD_FOLDER);
		} catch (SecurityException se) {
			return Response.status(500)
					.entity("Can not create destination folder on server")
					.build();
		}
                
                if(null != fileName && !"".equalsIgnoreCase(fileName)){
                    // write & upload file to server
                    inputStream = dataHandler.getInputStream();
                   fn= UPLOAD_FOLDER+"\\"+writeToFileServer(inputStream,UPLOAD_FOLDER,card+"_"+"_"+memberId+"_"+System.currentTimeMillis()+"."+extArr[1]);
 
                    // close the stream
                    inputStream.close();
                }
            }
            catch(IOException ioex) {
                ioex.printStackTrace();
            }
            finally {
                // release resources, if any
            }
        }
        return Response.ok(fn).build();
    }
 
    /**
     *
     * @param inputStream
     * @param fileName
     * @throws IOException
     */
    private String writeToFileServer(InputStream inputStream,String path, String fileName) throws IOException {
 
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(new File(path+"\\" +fileName));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            outputStream.flush();
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        finally{
            //release resource, if any
            outputStream.close();
        }
        return fileName;
    }
 
    /**
     *
     * @param multivaluedMap
     * @return
     */
    private String getFileName(MultivaluedMap<String, String> multivaluedMap) {
 
        String[] contentDisposition = multivaluedMap.getFirst("Content-Disposition").split(";");
        for (String filename : contentDisposition) {
 
            if ((filename.trim().startsWith("filename"))) {
                String[] name = filename.split("=");
                String exactFileName = name[1].trim().replaceAll("\"", "");
                return exactFileName;
            }
        }
        return "UnknownFile";
    }

    @Override
    public void showImage(HttpServletRequest request, HttpServletResponse response) {
       	try{
        String filename = request.getPathInfo();
        File dir = File.createTempFile("temp_", ".tmp");	
    	String absolutePath = dir.getAbsolutePath().substring(0,dir.getAbsolutePath().lastIndexOf(File.separator));
    	File sourceImage = new File(absolutePath+filename);
    	   BufferedImage image = ImageIO.read(sourceImage);
           OutputStream out = response.getOutputStream();
           ImageIO.write(image,"png", out);    	

        

		} catch (IOException e) {
			System.out.println(e);
			ClassUtil.getInstance().logError(logger, e);
		} catch (Exception e) {
		System.out.println(e);
		}
    }

    @Override
    public Response getImage(String image) {

      System.out.println("image path : "+image);
      File f = new File(Properties.getString("customer.upload.dir") + image);
      if (!f.exists()) {
        throw new WebApplicationException(404);
      }

      String mt = new MimetypesFileTypeMap().getContentType(f);
      ResponseBuilder responseBuilder = Response.ok(f, mt);

      return responseBuilder.build();
    }

    public String uploadMemberAvatar(Attachment memberAvatar, String memberName) throws IOException, ServiceException {
        String responsePath = Properties.getString("customer.upload.dir");
        String originalFileName = memberAvatar.getContentDisposition().getFilename();
        String[] ext = originalFileName.split("\\.");
        String filename = "ma_" + memberName.toLowerCase().replace(" ", "_") + "_" + System.currentTimeMillis() + "." + ext[1];

        try {
            java.nio.file.Path path = Paths.get(responsePath + filename);
            Files.deleteIfExists(path);
            InputStream in = memberAvatar.getObject(InputStream.class);

            Files.copy(in, path);
        } catch (Exception e) {
            throw new ServiceException(Constant.ResponseCode.EXCEPTION, Constant.ResponseMessage.MEMBER_AVATAR_FAILED);
        }

        return filename;
    }

      
}
