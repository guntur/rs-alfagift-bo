/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.v1.base.common;

import com.alfamart.alfagift_bo.v1.base.util.PasswordTools;

/**
 *
 * @author elko
 */
public class HashingService {
    public String hashingPassword(String password) {
 		return new PasswordTools().encryptPassword(password);
		
    }
}
