/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.v1.base.service;

import com.alfamart.alfagift_bo.v1.base.common.RestUrl;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;

/**
 *
 * @author Elko
 */

public interface FileUploadService {
    
    
    @GET
    @Path("/download/image")
    @Produces("image/png")
    public Response downloadImageFile();
 
    @POST
    @Path(RestUrl.V1.UPLOAD_IMAGES)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadImageFile(List<Attachment> attachments, @Context UriInfo ui,@PathParam("card") String card,@PathParam("memberId") String memberId);
    
    @GET
    @Path("/show/image/*")
    @Produces("image/png")
    public void showImage(@Context HttpServletRequest request,@Context HttpServletResponse response);
   
    @GET
    @Path("/show/images/{name}")
    @Produces("image/*")
    public Response getImage(@PathParam("name") String name);
}
