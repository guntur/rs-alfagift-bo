package com.alfamart.alfagift_bo.v1.base.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Properties {
	private static final String BUNDLE_NAME_STAGING = "staging";
	private static final ResourceBundle RESOURCE_BUNDLE_STAGING = ResourceBundle.getBundle(BUNDLE_NAME_STAGING);

	private static final String BUNDLE_NAME_VERIFY = "verify";
	private static final ResourceBundle RESOURCE_BUNDLE_VERIFY = ResourceBundle.getBundle(BUNDLE_NAME_VERIFY);

	private static final String BUNDLE_NAME_PRODUCTION = "production";
	private static final ResourceBundle RESOURCE_BUNDLE_PRODUCTION = ResourceBundle.getBundle(BUNDLE_NAME_PRODUCTION);
	
	private static final String BUNDLE_NAME_LOCAL = "local";
	private static final ResourceBundle RESOURCE_BUNDLE_LOCAL = ResourceBundle.getBundle(BUNDLE_NAME_LOCAL);
	
	
	public Properties() {
	}

	public static String getString(String key) {
		try {
			String strEnvirontment = System.getProperty("environtment");
			if ("verify".equals(strEnvirontment)) {
				return RESOURCE_BUNDLE_VERIFY.getString(key);
			} else if ("production".equals(strEnvirontment)) {
				return RESOURCE_BUNDLE_PRODUCTION.getString(key);
			} else if ("staging".equals(strEnvirontment)) {
				return RESOURCE_BUNDLE_STAGING.getString(key);
			} else if ("local".equals(strEnvirontment)){
				return RESOURCE_BUNDLE_LOCAL.getString(key);
			} else {
				return null;
			}
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
