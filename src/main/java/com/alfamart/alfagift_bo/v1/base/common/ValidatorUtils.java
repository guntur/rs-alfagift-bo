/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.v1.base.common;

import com.alfamart.alfagift_bo.v1.config.auth.TokenAuthentication;

import io.jsonwebtoken.Claims;

public class ValidatorUtils {
    
    public static boolean isTokenValid (String token, String id) {
        Claims claims = TokenAuthentication.validateToken(token);
        return claims != null ? claims.getId().equals(id) : false;
    }
   
}
