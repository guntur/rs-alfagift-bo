package com.alfamart.alfagift_bo.v1.base.util;

import java.util.Iterator;
import java.util.List;

public class ArrayUtil {
    public static <T> String implode(String glue, List<T> list) {
        if (list == null || list.isEmpty()) {
            return "";
        }

        Iterator<T> iter = list.iterator();

        StringBuilder sb = new StringBuilder();
        sb.append(iter.next());

        while (iter.hasNext()) {
            sb.append(glue).append(iter.next());
        }

        return sb.toString();
    }
}
