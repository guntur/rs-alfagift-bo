package com.alfamart.alfagift_bo.base.exception;

import com.alfamart.alfagift_bo.v1.base.model.Status;

public class CustomException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5040064884230531097L;
	private Status status;
	private String serviceName;
	
	
	public CustomException(String message)
	{
		super(message);
		
	}
	
	public CustomException(String message, Throwable cause)
	{
		super(message, cause);
	
	}	
	
	public CustomException(String serviceName, Status status)
	{
		this.status = status;
		this.serviceName = serviceName;
		
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	
	
	

}
