package com.alfamart.alfagift_bo.base.exception;

public class ServiceException extends Exception
{
	private static final long serialVersionUID = 356174818196868116L;
	
	private String code;
	
	public String getCode() {
		return code;
	}

	public ServiceException(String code, String message)
	{
		super(message);
		this.code = code;
	}
	
	public ServiceException(String code, String message, Throwable cause)
	{
		super(message, cause);
		this.code = code;
	}	
}