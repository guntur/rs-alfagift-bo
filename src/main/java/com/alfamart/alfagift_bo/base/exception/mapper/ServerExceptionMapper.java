package com.alfamart.alfagift_bo.base.exception.mapper;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerExceptionMapper implements ExceptionMapper<ServerErrorException> {
	
	public Logger logger = LoggerFactory.getLogger(this.getClass());
	
    @Override
    public Response toResponse(ServerErrorException e) {
    	
    	logger.info("ServerError Exception Response : INTERNAL SERVER ERROR");
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).header("Content-Type", "application/json").entity(new String("INTERNAL SERVER ERROR")).build();
    }
}
