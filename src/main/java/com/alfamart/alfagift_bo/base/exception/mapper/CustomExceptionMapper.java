package com.alfamart.alfagift_bo.base.exception.mapper;


import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import com.alfamart.alfagift_bo.base.exception.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class CustomExceptionMapper implements ExceptionMapper<CustomException>
{
	public Logger logger = LoggerFactory.getLogger(this.getClass());
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	  @Override
	    public Response toResponse(CustomException ce) {

			logger.info(ce.getServiceName() +" Response : "+gson.toJson(ce.getStatus()));
	        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).header("Content-Type", "application/json").entity(ce.getStatus()).build();
	    }

}
