package com.alfamart.alfagift_bo.base.exception.mapper;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientExceptionMapper implements ExceptionMapper<ClientErrorException> {
	
	public Logger logger = LoggerFactory.getLogger(this.getClass());
	
    @Override
    public Response toResponse(ClientErrorException e) {
    	
    	logger.info("Client Error Exception Response : SERVICE NOT FOUND");
        return Response.status(Response.Status.NOT_FOUND).header("Content-Type", "application/json").entity(new String("SERVICE NOT FOUND")).build();
    }
}
