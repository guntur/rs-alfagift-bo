package com.alfamart.alfagift_bo.base.config;

import com.alfamart.alfagift_bo.v1.base.util.Properties;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.TimeZone;

/**
 * Created by zer0, the Maverick Hunter
 * on 23/07/18.
 * Class: TimezoneConfigurator.java
 */
public class TimezoneConfigurator implements WebApplicationInitializer {

    private Logger logger = LoggerFactory.getLogger(TimezoneConfigurator.class);

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        if (!ValidatorUtil.isNullOrEmpty(Properties.getString("default.timezone"))) {
            logger.info("TIMEZONE CONFIGURATOR onStartup called");
            logger.info("Set Default Time Zone to : " + Properties.getString("default.timezone"));
            TimeZone.setDefault(TimeZone.getTimeZone(Properties.getString("default.timezone")));
        }

    }
}
