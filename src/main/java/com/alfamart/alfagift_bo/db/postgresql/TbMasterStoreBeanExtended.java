package com.alfamart.alfagift_bo.db.postgresql;

public class TbMasterStoreBeanExtended extends TbMasterStoreBean{
	private static final long serialVersionUID = 1L;
	private boolean favorite;

	public boolean getFavorite() {
		return favorite;
	}

	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}
}
