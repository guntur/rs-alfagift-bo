package com.alfamart.alfagift_bo.db.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alfamart.alfagift_bo.db.exception.DAOException;
import com.alfamart.alfagift_bo.db.exception.ObjectRetrievalException;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;

public class TbMasterStoreManagerExtended extends TbMasterStoreManager{
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	private static TbMasterStoreManagerExtended singleton = new TbMasterStoreManagerExtended();
	
	public static TbMasterStoreManagerExtended getInstance()
    {
        return singleton;
    }
	
	public List<TbMasterStoreBeanExtended> getListStoreFavorite(int memberId, String where) throws DAOException{
		List<TbMasterStoreBeanExtended> listView = new ArrayList<>();
		
		Connection c = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
		int member = !ValidatorUtil.isNullOrEmpty(memberId)?memberId:0;
	    String statement = "SELECT tbms.tbmstr_latitude, tbms.tbmstr_longitude, tbms.tbmstr_store_id, tbms.tbmstr_name, "
	    		+ "tbms.tbmstr_postcode,	tbms.tbmstr_branch,	tbms.tbmstr_telp,	tbms.tbmstr_village, "
	    		+ "tbms.tbmstr_district,	(	SELECT CASE	WHEN tbmsf.tbmmsf_id IS not NULL THEN	'1' "
	    		+ "END as favorite	FROM tb_master_member_store_favorite tbmsf "
	    		+ "WHERE	tbmsf.tbmstr_store_id = tbms.tbmstr_store_id "
	    		+ "AND tbmsf.tbmm_id = '" + Integer.toString(member) + "' LIMIT 1 "
	    		+ ") as is_favorite, tbms.tbmstr_address_1, tbms.tbmstr_address_2 FROM tb_master_store tbms ";
	    		
	    if(!ValidatorUtil.isNullOrEmpty(where)){
	    	statement = statement + " " + where;
	    }
	    
	    try{
	    	log.debug("getListStoreFavorite: " + statement);
	        System.out.println("getListStoreFavorite: " + statement);
	
	        c = this.getConnection();
	        int scrollType = ResultSet.TYPE_SCROLL_INSENSITIVE;
	        ps = c.prepareStatement(statement,
	                                scrollType,
	                                ResultSet.CONCUR_READ_ONLY);
	        rs = ps.executeQuery();
	        while(rs.next()){
	        	TbMasterStoreBeanExtended bean = new TbMasterStoreBeanExtended();
	        	bean.setTbmstrLatitude(rs.getString(1));
	        	bean.setTbmstrLongitude(rs.getString(2));
	        	bean.setTbmstrStoreId(rs.getString(3));
	        	bean.setTbmstrName(rs.getString(4));
	        	bean.setTbmstrPostcode(rs.getString(5));
	        	bean.setTbmstrBranch(rs.getString(6));
	        	bean.setTbmstrTelp(rs.getString(7));
	        	bean.setTbmstrVillage(rs.getString(8));
	        	bean.setTbmstrDistrict(rs.getString(9));
	        	bean.setFavorite(ValidatorUtil.isNullOrEmpty(rs.getString(10))?false:true);
	        	bean.setTbmstrAddress1(rs.getString(11));
	        	bean.setTbmstrAddress2(rs.getString(12));
	        	listView.add(bean);
	        }
	    }catch(SQLException e){
	    	throw new ObjectRetrievalException(e);
	    }finally{
	    	this.getManager().close(rs);
	        this.getManager().close(ps);
	        this.freeConnection(c);
	    }
	    
		return listView;
	}
	
	public List<TbMasterStoreBeanExtended> getListStoreFavoriteOnly(int memberId, String where, String where2) throws DAOException{
		List<TbMasterStoreBeanExtended> listView = new ArrayList<>();
		
		Connection c = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
		int member = !ValidatorUtil.isNullOrEmpty(memberId)?memberId:0;
	    String statement = "select * from (SELECT tbms.tbmstr_latitude, tbms.tbmstr_longitude, tbms.tbmstr_store_id, tbms.tbmstr_name, "
	    		+ "tbms.tbmstr_postcode,	tbms.tbmstr_branch,	tbms.tbmstr_telp,		tbms.tbmstr_village, "
	    		+ "tbms.tbmstr_district,	(	SELECT CASE	WHEN tbmsf.tbmmsf_id IS not NULL THEN	'1' "
	    		+ "END as favorite	FROM tb_master_member_store_favorite tbmsf "
	    		+ "WHERE	tbmsf.tbmstr_store_id = tbms.tbmstr_store_id "
	    		+ "AND tbmsf.tbmm_id = '" + Integer.toString(member) + "' LIMIT 1 "
	    		+ ") as is_favorite, tbms.tbmstr_address_1, tbms.tbmstr_address_2 FROM tb_master_store tbms ";
	    		
	    if(!ValidatorUtil.isNullOrEmpty(where)){
	    	statement = statement + " " + where + " ) view_favorite";
	    }else{
	    	statement = statement + " ) view_favorite";
	    }
	    
	    if(!ValidatorUtil.isNullOrEmpty(where2)){
	    	statement = statement + " " + where2;
	    }
	    
	    try{
	    	log.debug("getListStoreFavoriteOnly: " + statement);
	        System.out.println("getListStoreFavoriteOnly: " + statement);
	
	        c = this.getConnection();
	        int scrollType = ResultSet.TYPE_SCROLL_INSENSITIVE;
	        ps = c.prepareStatement(statement,
	                                scrollType,
	                                ResultSet.CONCUR_READ_ONLY);
	        rs = ps.executeQuery();
	        while(rs.next()){
	        	TbMasterStoreBeanExtended bean = new TbMasterStoreBeanExtended();
	        	bean.setTbmstrLatitude(rs.getString(1));
	        	bean.setTbmstrLongitude(rs.getString(2));
	        	bean.setTbmstrStoreId(rs.getString(3));
	        	bean.setTbmstrName(rs.getString(4));
	        	bean.setTbmstrPostcode(rs.getString(5));
	        	bean.setTbmstrBranch(rs.getString(6));
	        	bean.setTbmstrTelp(rs.getString(7));
	        	bean.setTbmstrVillage(rs.getString(8));
	        	bean.setTbmstrDistrict(rs.getString(9));
	        	bean.setFavorite(ValidatorUtil.isNullOrEmpty(rs.getString(10))?false:true);
	        	bean.setTbmstrAddress1(rs.getString(11));
	        	bean.setTbmstrAddress2(rs.getString(12));
	        	listView.add(bean);
	        }
	    }catch(SQLException e){
	    	throw new ObjectRetrievalException(e);
	    }finally{
	    	this.getManager().close(rs);
	        this.getManager().close(ps);
	        this.freeConnection(c);
	    }
	    
		return listView;
	}
}
