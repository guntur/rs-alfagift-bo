package com.alfamart.alfagift_bo.db.mongo.repository;

import com.alfamart.alfagift_bo.db.mongo.domain.Brand;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by zer0, the Maverick Hunter
 * on 27/07/18.
 * Class: BrandRepository.java
 */
public interface BrandRepository extends MongoRepository<Brand, String> {
}
