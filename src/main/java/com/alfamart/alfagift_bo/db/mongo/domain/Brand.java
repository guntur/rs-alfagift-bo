package com.alfamart.alfagift_bo.db.mongo.domain;

import com.alfamart.alfagift_bo.v1.promotion.model.BrandPromotion;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Document(collection = "alfagift_master_brand")
public class Brand extends BaseDomain implements Serializable {

    private String brandName;
    private String imagePath;
    private Integer sortNo;
    private Integer status;

    public Brand() {
    }

    public Brand(BrandPromotion brandPromotion) {
        this.brandName = brandPromotion.getBrandName();
        this.imagePath = brandPromotion.getImagePath();
        this.sortNo = brandPromotion.getSortNo();
        this.status = brandPromotion.getStatus();
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
