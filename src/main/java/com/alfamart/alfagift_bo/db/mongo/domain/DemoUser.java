package com.alfamart.alfagift_bo.db.mongo.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by zer0, the Maverick Hunter
 * on 26/07/18.
 * Class: DemoUser.java
 */
@ApiModel
public class DemoUser {

    String firstName;
    String LastName;
    String email;
    String password;
    @ApiModelProperty(value = "phone number MUST start with leading 62!")
    String phone;

    public DemoUser() {}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
