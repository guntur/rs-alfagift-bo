package com.alfamart.alfagift_bo.db.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by zer0, the Maverick Hunter
 * on 28/07/18.
 * Class: PontaToken.java
 */
@Document(collection = "alfagift_ponta_token")
public class PontaToken extends BaseDomain {

    private String trxId;
    private String token;
    private Boolean status;

    public PontaToken(String trxId, String token, Boolean status) {
        this.trxId = trxId;
        this.token = token;
        this.status = status;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
