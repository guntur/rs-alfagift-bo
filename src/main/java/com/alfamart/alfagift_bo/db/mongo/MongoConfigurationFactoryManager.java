package com.alfamart.alfagift_bo.db.mongo;

import com.alfamart.alfagift_bo.v1.base.util.Properties;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by zer0, the Maverick Hunter
 * on 22/07/18.
 * Class: MongoFactoryManager.java
 */
@Configuration
@EnableMongoRepositories(basePackages = "com.alfamart.alfagift_bo.db.mongo.repository")
public class MongoConfigurationFactoryManager extends AbstractMongoConfiguration {

    @Override
    protected String getDatabaseName() {
        return Properties.getString("mongo.alfagift.database");
    }

    @Bean
    public MongoDbFactory mongoDbFactory() {
        MongoClientURI mongoClientURI = new MongoClientURI(mongoUriBuilder());
        return new SimpleMongoDbFactory(mongoClientURI);
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongoDbFactory());
    }

    @Override
    public MongoClient mongoClient() {
        MongoClient mongoClient = new MongoClient();
        mongoClient.getDatabase(getDatabaseName());
        return mongoClient;
    }

    private String mongoUriBuilder() {
        String uri;

        if (!ValidatorUtil.isNullOrEmpty(Properties.getString("mongo.alfagift.username"))) {
            uri = "mongodb://"+
                    Properties.getString("mongo.alfagift.username") + ":" +
                    Properties.getString("mongo.alfagift.password") + "@" +
                    Properties.getString("mongo.alfagift.host") + ":" +
                    Properties.getString("mongo.alfagift.port") + "/" +
                    Properties.getString("mongo.alfagift.database") + "?readPreference=primary";

        } else {
            uri = "mongodb://"+
                    Properties.getString("mongo.alfagift.host") + ":" +
                    Properties.getString("mongo.alfagift.port") + "/" +
                    Properties.getString("mongo.alfagift.database");
        }

        return uri;
    }
}
