package com.alfamart.alfagift_bo.db.mongo.repository;

import com.alfamart.alfagift_bo.db.mongo.domain.PontaToken;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by zer0, the Maverick Hunter
 * on 28/07/18.
 * Class: PontaTokenRepository.java
 */
public interface PontaTokenRepository extends MongoRepository<PontaToken, String> {

    PontaToken findByStatus(Boolean status);

}
