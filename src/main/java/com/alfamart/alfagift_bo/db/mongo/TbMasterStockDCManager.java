package com.alfamart.alfagift_bo.db.mongo;

import com.alfamart.alfagift_bo.v1.base.util.Properties;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;
import com.google.gson.Gson;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public final class TbMasterStockDCManager {

    private static TbMasterStockDCManager instance = new TbMasterStockDCManager();

    private static Logger log = LoggerFactory.getLogger(TbMasterStockDCManager.class);

    private static MongoDatabase db;
    private static String collectionName = "tb_master_stock_dc";
    private static MongoCollection<Document> collection;
    private static MongoClient client;
    private static String host = Properties.getString("mongo.host");
    private static String user = Properties.getString("mongo.user");
    private static String password = Properties.getString("mongo.password");
    private static String database = Properties.getString("mongo.database");

    public String _ID = "_id";
    public String PRODUCT_ID = "product_id";
    public String SKU_SELLER = "sku_seller";
    public String STOCK_AVAILABLE = "stock_available";
    public String STORE_ID = "store_id";
    public String CREATE_DATE = "create_date";
    public String UPDATE_DATE = "update_date";
    public String CREATE_ID = "create_id";
    public String UPDATE_ID = "update_id";
    public String TAG_PROD = "tag_prod";


    private TbMasterStockDCManager() {
        return;
    }

    public static TbMasterStockDCManager getInstance(){
        client = new MongoClient(new MongoClientURI("mongodb://" + user + ":" + password + "@" + host + "/" + database + "?readPreference=primary"));
        db = client.getDatabase(database);
        collection = db.getCollection(collectionName);
        return instance;
    }

    public TbMasterStockDCBean[] load(Integer limit){
        final List<TbMasterStockDCBean> _result = new ArrayList<>();

        try {
            Block<Document> printBlock = new Block<Document>() {
                @Override
                public void apply(final Document document) {
                    TbMasterStockDCBean stockDCBean = new TbMasterStockDCBean();
//                    stockDCBean.setProductId(document.getString(PRODUCT_ID));
                    stockDCBean.setSkuSeller(document.getString(SKU_SELLER));
                    stockDCBean.setStockAvailable(document.getInteger(STOCK_AVAILABLE));
                    stockDCBean.setStoreId(document.getString(STORE_ID));
                    stockDCBean.setCreateDate(document.getDate(CREATE_DATE));
                    stockDCBean.setUpdateDate(document.getDate(UPDATE_DATE));
                    stockDCBean.setCreateId(document.getInteger(CREATE_ID));
                    stockDCBean.setUpdateId(document.getInteger(UPDATE_ID));
                    stockDCBean.setTagProd(document.getString(TAG_PROD));
                    _result.add(stockDCBean);
                }
            };

            if(limit != null){
                collection.find().limit(limit).forEach(printBlock);
            }else{
                collection.find().forEach(printBlock);
            }
        } catch (MongoException e) {
            log.error("Error query to Db :: " + e.getMessage());
        }

        TbMasterStockDCBean[] result = new TbMasterStockDCBean[_result.size()];
        TbMasterStockDCBean[] finalResult = _result.toArray(result);
        log.info("Stock DC :: " + new Gson().toJson(finalResult));

        client.close();
        return finalResult;
    }

    public TbMasterStockDCBean[] loadByDocument(Document query) {
        final List<TbMasterStockDCBean> _result = new ArrayList<>();

        try {
            Block<Document> printBlock = new Block<Document>() {
                @Override
                public void apply(final Document document) {
                    TbMasterStockDCBean stockDCBean = new TbMasterStockDCBean();
//                    stockDCBean.setProductId(document.getString(PRODUCT_ID));
                    stockDCBean.setSkuSeller(document.getString(SKU_SELLER));
                    stockDCBean.setStockAvailable(document.getInteger(STOCK_AVAILABLE));
                    stockDCBean.setStoreId(document.getString(STORE_ID));
                    stockDCBean.setCreateDate(document.getDate(CREATE_DATE));
                    stockDCBean.setUpdateDate(document.getDate(UPDATE_DATE));
                    stockDCBean.setCreateId(document.getInteger(CREATE_ID));
                    stockDCBean.setUpdateId(document.getInteger(UPDATE_ID));
                    stockDCBean.setTagProd(document.getString(TAG_PROD));
                    stockDCBean.setOid(document.getObjectId(_ID));
                    _result.add(stockDCBean);
                }
            };
            collection.find(query).forEach(printBlock);
        } catch (MongoException e) {
            log.error("Error query to DB :: " + e.getMessage());
        }

        TbMasterStockDCBean[] result = new TbMasterStockDCBean[_result.size()];
        TbMasterStockDCBean[] finalResult = _result.toArray(result);
        log.info("Stock DC :: " + new Gson().toJson(finalResult));

            client.close();
        return finalResult;
    }

    public TbMasterStockDCBean loadUniqueByDocument(Document query) {
        final List<TbMasterStockDCBean> _result = new ArrayList<>();

        try {
            Block<Document> printBlock = new Block<Document>() {
                @Override
                public void apply(final Document document) {
                    TbMasterStockDCBean stockDCBean = new TbMasterStockDCBean();
//                    stockDCBean.setProductId(document.getString(PRODUCT_ID));
                    stockDCBean.setSkuSeller(document.getString(SKU_SELLER));
                    stockDCBean.setStockAvailable(document.getInteger(STOCK_AVAILABLE));
                    stockDCBean.setStoreId(document.getString(STORE_ID));
                    stockDCBean.setCreateDate(document.getDate(CREATE_DATE));
                    stockDCBean.setUpdateDate(document.getDate(UPDATE_DATE));
                    stockDCBean.setCreateId(document.getInteger(CREATE_ID));
                    stockDCBean.setUpdateId(document.getInteger(UPDATE_ID));
                    stockDCBean.setTagProd(document.getString(TAG_PROD));
                    _result.add(stockDCBean);
                }
            };
            collection.find(query).forEach(printBlock);
        } catch (MongoException e) {
            log.error("Error query to DB :: " + e.getMessage());
        }

        if(_result.size() > 1){
            client.close();
            throw new MongoException("Result more than one element !!");
        }else if(_result.size() == 0){
            client.close();
            return null;
        }

        TbMasterStockDCBean result = _result.get(0);

        client.close();
        return result;
    }

    public String insert(TbMasterStockDCBean bean){
        String _id = null;
        try {
            Document doc = new Document(PRODUCT_ID, bean.getProductId())
                    .append(SKU_SELLER, bean.getSkuSeller())
                    .append(STOCK_AVAILABLE, bean.getStockAvailable())
                    .append(STORE_ID, bean.getStoreId())
                    .append(CREATE_DATE, bean.getCreateDate())
                    .append(UPDATE_DATE, bean.getUpdateDate())
                    .append(CREATE_ID, bean.getCreateId())
                    .append(UPDATE_ID, bean.getUpdateId())
                    .append(TAG_PROD, bean.getTagProd());
            collection.insertOne(doc);

            _id = doc.getObjectId(_ID).toString();
        } catch (MongoException e) {
            log.error("Error get ");
        }

        client.close();
        return _id;
    }

    public boolean insert(TbMasterStockDCBean[] beans){

        boolean success = false;
        try {
            List<Document> docs = new ArrayList<Document>();
            for (TbMasterStockDCBean bean : beans) {
                Document doc = new Document("product_id", bean.getProductId())
                        .append(SKU_SELLER, bean.getSkuSeller())
                        .append(STOCK_AVAILABLE, bean.getStockAvailable())
                        .append(STORE_ID, bean.getStoreId())
                        .append(CREATE_DATE, bean.getCreateDate())
                        .append(UPDATE_DATE, bean.getUpdateDate())
                        .append(CREATE_ID, bean.getCreateId())
                        .append(UPDATE_ID, bean.getUpdateId())
                        .append(TAG_PROD, bean.getTagProd());
                docs.add(doc);
            }

            collection.insertMany(docs);
            success = true;
        } catch (MongoException e) {
            log.error("Error insert to DB :: " + e.getMessage());
        }

        client.close();
        return success;
    }

    public boolean updateByTemplate(TbMasterStockDCBean bean) {
        boolean success = false;
        try {
            Bson filter = new Document(_ID, bean.getOid());

            Document newValue = new Document();

            if (!ValidatorUtil.isNullOrEmpty(bean.getCreateDate())) {
                newValue.append(CREATE_DATE, bean.getCreateDate());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getUpdateDate())) {
                newValue.append(UPDATE_DATE, bean.getUpdateDate());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getCreateId())) {
                newValue.append(CREATE_ID, bean.getCreateId());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getProductId())) {
                newValue.append(PRODUCT_ID, bean.getProductId());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getSkuSeller())) {
                newValue.append(SKU_SELLER, bean.getSkuSeller());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getStockAvailable())) {
                newValue.append(STOCK_AVAILABLE, bean.getStockAvailable());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getStoreId())) {
                newValue.append(STORE_ID, bean.getStoreId());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getTagProd())) {
                newValue.append(TAG_PROD, bean.getTagProd());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getUpdateId())) {
                newValue.append(UPDATE_ID, bean.getUpdateId());
            }

            Bson updateNewValue = new Document("$set", newValue);

            collection.updateOne(filter, updateNewValue);
            success = true;
        } catch (MongoException e) {
            log.error("Error update data " + e.getMessage());
        }

        client.close();
        return success;
    }

    @Override
    protected void finalize() throws Throwable {
        client.close();
    }

}

