package com.alfamart.alfagift_bo.db.mongo.repository;

import com.alfamart.alfagift_bo.db.mongo.domain.Otp;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by zer0, the Maverick Hunter
 * on 22/07/18.
 * Interface: OtpRepository.java
 */
@Repository
public interface OtpRepository extends MongoRepository<Otp, String> {

    // @Query("{domain: { $regex: ?0 } })")
    Otp findByPhoneNumAndOtpCodeAndActionAndValidUntilGreaterThanEqual(String phoneNum, Long otpCode, String action, Date now);
    Otp findByPhoneNumAndOtpCodeAndAction(String phoneNum, Long otpCode, String action);

    List<Otp> findByPhoneNumAndAction(String phoneNum, String action);
}
