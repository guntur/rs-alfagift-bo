package com.alfamart.alfagift_bo.db.mongo.repository;

import com.alfamart.alfagift_bo.db.mongo.domain.Campaign;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by zer0, the Maverick Hunter
 * on 27/07/18.
 * Class: CampaignRepository.java
 */
public interface CampaignRepository extends MongoRepository<Campaign, String> {
}
