package com.alfamart.alfagift_bo.db.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "alfagift_master_extra_point")
public class ExtraPoint extends BaseDomain implements Serializable {

    private String extraPointTitle;
    private String campaignId;
    private String pontaPoint;
    private Integer alfaStar;
    private Integer status;

    public ExtraPoint(){}

    public String getExtraPointTitle() {
        return extraPointTitle;
    }

    public void setExtraPointTitle(String extraPointTitle) {
        this.extraPointTitle = extraPointTitle;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getPontaPoint() {
        return pontaPoint;
    }

    public void setPontaPoint(String pontaPoint) {
        this.pontaPoint = pontaPoint;
    }

    public Integer getAlfaStar() {
        return alfaStar;
    }

    public void setAlfaStar(Integer alfaStar) {
        this.alfaStar = alfaStar;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
