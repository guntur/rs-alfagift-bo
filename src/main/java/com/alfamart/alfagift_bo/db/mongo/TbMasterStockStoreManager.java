package com.alfamart.alfagift_bo.db.mongo;

import com.alfamart.alfagift_bo.v1.base.util.Properties;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;
import com.google.gson.Gson;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public final class TbMasterStockStoreManager {

    private static TbMasterStockStoreManager instance = new TbMasterStockStoreManager();

    private static Logger log = LoggerFactory.getLogger(TbMasterStockStoreManager.class);

    private static MongoDatabase db;
    private static String collectionName = "tb_master_stock_store";
    private static MongoCollection<Document> collection;
    private static MongoClient client;
    private static String host = Properties.getString("mongo.host");
    private static String user = Properties.getString("mongo.user");
    private static String password = Properties.getString("mongo.password");
    private static String database = Properties.getString("mongo.database");

    public String _ID = "_id";
    public String CREATE_DATE = "create_date";
    public String UPDATE_DATE = "update_date";
    public String CREATE_ID = "create_id";
    public String UPDATE_ID = "update_id";
    public String PRODUCT_ID = "product_id";
    public String SKU_SELLER = "sku_seller";
    public String STORE_ID = "store_id";
    public String EOD = "eod";
    public String TAG_PROD = "tag_prod";
    public String ACTIVE = "active";
    public String STOCK_AVAILABLE = "stock_available";

    private TbMasterStockStoreManager() {
        return;
    }

    public static TbMasterStockStoreManager getInstance() {
        client = new MongoClient(new MongoClientURI("mongodb://" + user + ":" + password + "@" + host + "/" + database + "?readPreference=primary"));
        db = client.getDatabase(database);
        collection = db.getCollection(collectionName);

        return instance;
    }

    public TbMasterStockStoreBean[] load(Integer limit) {
        final List<TbMasterStockStoreBean> _result = new ArrayList<>();

        try {
            Block<Document> printBlock = new Block<Document>() {
                @Override
                public void apply(Document document) {
                    TbMasterStockStoreBean stockStoreBean = new TbMasterStockStoreBean();
                    stockStoreBean.setActive(document.getString(ACTIVE));
                    stockStoreBean.setCreateDate(document.getDate(CREATE_DATE));
                    stockStoreBean.setCreateId(document.getInteger(CREATE_ID));
                    stockStoreBean.setEod(document.getDate(EOD));
                    stockStoreBean.setOid(document.getObjectId(_ID));
//                    stockStoreBean.setProductId(document.getString(PRODUCT_ID));
                    stockStoreBean.setSkuSeller(document.getString(SKU_SELLER));
                    stockStoreBean.setStockAvaibility(document.getInteger(STOCK_AVAILABLE));
                    stockStoreBean.setStoreId(document.getString(STORE_ID));
                    stockStoreBean.setTagProd(document.getString(TAG_PROD));
                    stockStoreBean.setUpdateDate(document.getDate(UPDATE_DATE));
                    stockStoreBean.setUpdateId(document.getInteger(UPDATE_ID));
                    _result.add(stockStoreBean);
                }
            };

            if (limit != null) {
                collection.find().limit(limit).forEach(printBlock);
            } else {
                collection.find().forEach(printBlock);
            }
        } catch (MongoException mongE) {
            log.error("Error query to mongo :: " + mongE.getMessage());
        }


        TbMasterStockStoreBean[] result = new TbMasterStockStoreBean[_result.size()];

        client.close();
        return _result.toArray(result);
    }

    public TbMasterStockStoreBean[] loadByDocument(Document query) {
        final List<TbMasterStockStoreBean> _result = new ArrayList<>();

        try {
            Block<Document> processBlock = new Block<Document>() {
                @Override
                public void apply(Document document) {

                    TbMasterStockStoreBean stockStoreBean = new TbMasterStockStoreBean();
                    stockStoreBean.setActive(document.getString(ACTIVE));
                    stockStoreBean.setCreateDate(document.getDate(CREATE_DATE));
                    stockStoreBean.setCreateId(document.getInteger(CREATE_ID));
                    stockStoreBean.setEod(document.getDate(EOD));
                    stockStoreBean.setOid(document.getObjectId(_ID));
//                    stockStoreBean.setProductId(document.getString(PRODUCT_ID));
                    stockStoreBean.setSkuSeller(document.getString(SKU_SELLER));
                    stockStoreBean.setStockAvaibility(document.getInteger(STOCK_AVAILABLE));
                    stockStoreBean.setStoreId(document.getString(STORE_ID));
                    stockStoreBean.setTagProd(document.getString(TAG_PROD));
                    stockStoreBean.setUpdateDate(document.getDate(UPDATE_DATE));
                    stockStoreBean.setUpdateId(document.getInteger(UPDATE_ID));
                    _result.add(stockStoreBean);
                }

            };

            collection.find(query).forEach(processBlock);
        } catch (MongoException mongE) {
            log.error("Error query to DB " + mongE.getMessage());
        }
        TbMasterStockStoreBean[] result = new TbMasterStockStoreBean[_result.size()];
        TbMasterStockStoreBean[] finalResult = _result.toArray(result);
        log.info("Stock Store :: " + new Gson().toJson(finalResult));

        client.close();
        return finalResult;

    }

    public TbMasterStockStoreBean loadUniqueByDocument(Document query) {
        final List<TbMasterStockStoreBean> _result = new ArrayList<>();

        try {
            Block<Document> processBlock = new Block<Document>() {
                @Override
                public void apply(Document document) {

                    TbMasterStockStoreBean stockStoreBean = new TbMasterStockStoreBean();
                    stockStoreBean.setActive(document.getString(ACTIVE));
                    stockStoreBean.setCreateDate(document.getDate(CREATE_DATE));
                    stockStoreBean.setCreateId(document.getInteger(CREATE_ID));
                    stockStoreBean.setEod(document.getDate(EOD));
                    stockStoreBean.setOid(document.getObjectId(_ID));
//                    stockStoreBean.setProductId(document.getInteger(PRODUCT_ID));
                    stockStoreBean.setSkuSeller(document.getString(SKU_SELLER));
                    stockStoreBean.setStockAvaibility(document.getInteger(STOCK_AVAILABLE));
                    stockStoreBean.setStoreId(document.getString(STORE_ID));
                    stockStoreBean.setTagProd(document.getString(TAG_PROD));
                    stockStoreBean.setUpdateDate(document.getDate(UPDATE_DATE));
                    stockStoreBean.setUpdateId(document.getInteger(UPDATE_ID));
                    _result.add(stockStoreBean);
                }

            };

            collection.find(query).forEach(processBlock);
        } catch (MongoException mongE) {
            log.error("Error query to DB " + mongE.getMessage());
        }

        if(_result.size() > 1){
            throw new MongoException("Result more than one element !!");
        }else if(_result.size() == 0){
            return null;
        }

        TbMasterStockStoreBean result = _result.get(0);

        client.close();
        return result;

    }

    public String insert(TbMasterStockStoreBean bean) {

        String objectId = null;
        try {
            Document queryInsert = new Document();

            if (!ValidatorUtil.isNullOrEmpty(bean.getUpdateId())) {
                queryInsert.append(UPDATE_ID, bean.getUpdateId());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getTagProd())) {
                queryInsert.append(TAG_PROD, bean.getTagProd());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getStoreId())) {
                queryInsert.append(STORE_ID, bean.getStoreId());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getStockAvaibility())) {
                queryInsert.append(STOCK_AVAILABLE, bean.getStockAvaibility());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getSkuSeller())) {
                queryInsert.append(STOCK_AVAILABLE, bean.getSkuSeller());
            }

            /*if (!ValidatorUtil.isNullOrEmpty(bean.getProductId())) {
                queryInsert.append(PRODUCT_ID, bean.getProductId());
            }*/

            if (!ValidatorUtil.isNullOrEmpty(bean.getEod())) {
                queryInsert.append(EOD, bean.getEod());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getCreateId())) {
                queryInsert.append(CREATE_ID, bean.getCreateId());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getActive())) {
                queryInsert.append(ACTIVE, bean.getActive());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getUpdateDate())) {
                queryInsert.append(UPDATE_DATE, bean.getUpdateDate());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getCreateDate())) {
                queryInsert.append(UPDATE_DATE, bean.getUpdateDate());
            }

            collection.insertOne(queryInsert);
            objectId = queryInsert.getObjectId(_ID).toString();
        } catch (MongoException e) {
            log.error("Error insert One :: " + e.getMessage());
        }

        client.close();
        return objectId;
    }

    public boolean insert(TbMasterStockStoreBean[] beans) {
        List<Document> manyQueryInsert = new ArrayList<>();

        boolean success = false;
        try {
            for (TbMasterStockStoreBean bean : beans) {

                Document queryInsert = new Document();

                if (!ValidatorUtil.isNullOrEmpty(bean.getUpdateId())) {
                    queryInsert.append(UPDATE_ID, bean.getUpdateId());
                }

                if (!ValidatorUtil.isNullOrEmpty(bean.getTagProd())) {
                    queryInsert.append(TAG_PROD, bean.getTagProd());
                }

                if (!ValidatorUtil.isNullOrEmpty(bean.getStoreId())) {
                    queryInsert.append(STORE_ID, bean.getStoreId());
                }

                if (!ValidatorUtil.isNullOrEmpty(bean.getStockAvaibility())) {
                    queryInsert.append(STOCK_AVAILABLE, bean.getStockAvaibility());
                }

                if (!ValidatorUtil.isNullOrEmpty(bean.getSkuSeller())) {
                    queryInsert.append(STOCK_AVAILABLE, bean.getSkuSeller());
                }

                /*if (!ValidatorUtil.isNullOrEmpty(bean.getProductId())) {
                    queryInsert.append(PRODUCT_ID, bean.getProductId());
                }*/

                if (!ValidatorUtil.isNullOrEmpty(bean.getEod())) {
                    queryInsert.append(EOD, bean.getEod());
                }

                if (!ValidatorUtil.isNullOrEmpty(bean.getCreateId())) {
                    queryInsert.append(CREATE_ID, bean.getCreateId());
                }

                if (!ValidatorUtil.isNullOrEmpty(bean.getActive())) {
                    queryInsert.append(ACTIVE, bean.getActive());
                }

                if (!ValidatorUtil.isNullOrEmpty(bean.getUpdateDate())) {
                    queryInsert.append(UPDATE_DATE, bean.getUpdateDate());
                }

                if (!ValidatorUtil.isNullOrEmpty(bean.getCreateDate())) {
                    queryInsert.append(UPDATE_DATE, bean.getUpdateDate());
                }
                manyQueryInsert.add(queryInsert);
            }

            collection.insertMany(manyQueryInsert);
            success = true;
        } catch (MongoException mongEx) {
            log.error("Error insert many :: " + mongEx.getMessage());
        }

        client.close();
        return success;
    }

    public boolean updateByTemplate(TbMasterStockStoreBean bean) {
        boolean success = false;

        try {
            Bson filter = new Document(_ID, bean.getOid());
//            filter.append(_ID, bean.getOid());

            Document newValue = new Document();
            if (!ValidatorUtil.isNullOrEmpty(bean.getCreateDate())) {
                newValue.append(CREATE_DATE, bean.getCreateDate());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getUpdateDate())) {
                newValue.append(UPDATE_DATE, bean.getUpdateDate());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getActive())) {
                newValue.append(ACTIVE, bean.getActive());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getCreateId())) {
                newValue.append(CREATE_ID, bean.getCreateId());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getEod())) {
                newValue.append(EOD, bean.getEod());
            }

            /*if (!ValidatorUtil.isNullOrEmpty(bean.getProductId())) {
                newValue.append(PRODUCT_ID, bean.getProductId());
            }*/

            if (!ValidatorUtil.isNullOrEmpty(bean.getSkuSeller())) {
                newValue.append(SKU_SELLER, bean.getSkuSeller());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getStockAvaibility())) {
                newValue.append(STOCK_AVAILABLE, bean.getStockAvaibility());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getStoreId())) {
                newValue.append(STORE_ID, bean.getStoreId());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getTagProd())) {
                newValue.append(TAG_PROD, bean.getTagProd());
            }

            if (!ValidatorUtil.isNullOrEmpty(bean.getUpdateId())) {
                newValue.append(UPDATE_ID, bean.getUpdateId());
            }

            Bson updateValue = new Document("$set", newValue);

            collection.updateOne(filter, updateValue);

            success = true;
        } catch (MongoException mongEx) {
            log.error("Error update data " + mongEx.getMessage());
        }
        client.close();
        return success;
    }

    @Override
    protected void finalize() throws Throwable {
        client.close();
    }

}
