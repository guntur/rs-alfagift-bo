package com.alfamart.alfagift_bo.db.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by zer0, the Maverick Hunter
 * on 06/08/18.
 * Class: Interest.java
 */
@Document(collection = "alfagift_master_interest")
public class Interest extends BaseDomain {

    private String title;
    private String webImagePath;
    private String appImagePath;
    private Integer status;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebImagePath() {
        return webImagePath;
    }

    public void setWebImagePath(String webImagePath) {
        this.webImagePath = webImagePath;
    }

    public String getAppImagePath() {
        return appImagePath;
    }

    public void setAppImagePath(String appImagePath) {
        this.appImagePath = appImagePath;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
