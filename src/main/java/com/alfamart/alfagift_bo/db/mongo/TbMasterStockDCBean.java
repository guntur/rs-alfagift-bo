package com.alfamart.alfagift_bo.db.mongo;

import org.bson.types.ObjectId;

import java.util.Date;

public class TbMasterStockDCBean {
    private ObjectId oid;
    private Date createDate;
    private Date updateDate;
    private Integer createId;
    private Integer updateId;
    private String productId;
    private String skuSeller;
    private String storeId;
    private Integer stockAvailable;
    private String tagProd;

    public ObjectId getOid() {
        return oid;
    }

    public void setOid(ObjectId oid) {
        this.oid = oid;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getCreateId() {
        return createId;
    }

    public void setCreateId(Integer createId) {
        this.createId = createId;
    }

    public Integer getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Integer updateId) {
        this.updateId = updateId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSkuSeller() {
        return skuSeller;
    }

    public void setSkuSeller(String skuSeller) {
        this.skuSeller = skuSeller;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public Integer getStockAvailable() {
        return stockAvailable;
    }

    public void setStockAvailable(Integer stockAvailable) {
        this.stockAvailable = stockAvailable;
    }

    public String getTagProd() {
        return tagProd;
    }

    public void setTagProd(String tagProd) {
        this.tagProd = tagProd;
    }
}

