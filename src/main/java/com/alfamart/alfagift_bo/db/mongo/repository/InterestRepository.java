package com.alfamart.alfagift_bo.db.mongo.repository;

import com.alfamart.alfagift_bo.db.mongo.domain.Interest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zer0, the Maverick Hunter
 * on 06/08/18.
 * Class: InterestRepository.java
 */
@Repository
public interface InterestRepository extends MongoRepository<Interest, String> {

    List<Interest> findByStatus(Integer status);

}
