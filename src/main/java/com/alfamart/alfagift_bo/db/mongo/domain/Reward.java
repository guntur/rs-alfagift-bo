package com.alfamart.alfagift_bo.db.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Document(collection = "alfagift_master_reward")
public class Reward extends BaseDomain implements Serializable {

    private String rewardId;
    private String rewardName;
    private String campaignId;
    private String categoryId;
    private String rewardEndDate;
    private String rewardDescription;
    private String rewardTermCond;
    private MultipartFile webImage;
    private String webImagePath;
    private MultipartFile appImage;
    private String appImagePath;
    private Integer status;
    private String voucherLabel;
    private String voucherClaim;
    private String voucherClaimType;
    private String voucherUser;
    private MultipartFile voucherFileXls;
    private String voucherFileXlsPath;

    public Reward(){}

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getRewardEndDate() {
        return rewardEndDate;
    }

    public void setRewardEndDate(String rewardEndDate) {
        this.rewardEndDate = rewardEndDate;
    }

    public String getRewardDescription() {
        return rewardDescription;
    }

    public void setRewardDescription(String rewardDescription) {
        this.rewardDescription = rewardDescription;
    }

    public String getRewardTermCond() {
        return rewardTermCond;
    }

    public void setRewardTermCond(String rewardTermCond) {
        this.rewardTermCond = rewardTermCond;
    }

    public MultipartFile getWebImage() {
        return webImage;
    }

    public void setWebImage(MultipartFile webImage) {
        this.webImage = webImage;
    }

    public String getWebImagePath() {
        return webImagePath;
    }

    public void setWebImagePath(String webImagePath) {
        this.webImagePath = webImagePath;
    }

    public MultipartFile getAppImage() {
        return appImage;
    }

    public void setAppImage(MultipartFile appImage) {
        this.appImage = appImage;
    }

    public String getAppImagePath() {
        return appImagePath;
    }

    public void setAppImagePath(String appImagePath) {
        this.appImagePath = appImagePath;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getVoucherLabel() {
        return voucherLabel;
    }

    public void setVoucherLabel(String voucherLabel) {
        this.voucherLabel = voucherLabel;
    }

    public String getVoucherClaim() {
        return voucherClaim;
    }

    public void setVoucherClaim(String voucherClaim) {
        this.voucherClaim = voucherClaim;
    }

    public String getVoucherClaimType() {
        return voucherClaimType;
    }

    public void setVoucherClaimType(String voucherClaimType) {
        this.voucherClaimType = voucherClaimType;
    }

    public String getVoucherUser() {
        return voucherUser;
    }

    public void setVoucherUser(String voucherUser) {
        this.voucherUser = voucherUser;
    }

    public MultipartFile getVoucherFileXls() {
        return voucherFileXls;
    }

    public void setVoucherFileXls(MultipartFile voucherFileXls) {
        this.voucherFileXls = voucherFileXls;
    }

    public String getVoucherFileXlsPath() {
        return voucherFileXlsPath;
    }

    public void setVoucherFileXlsPath(String voucherFileXlsPath) {
        this.voucherFileXlsPath = voucherFileXlsPath;
    }
}
