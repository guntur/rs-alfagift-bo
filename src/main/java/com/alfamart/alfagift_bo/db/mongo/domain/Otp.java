package com.alfamart.alfagift_bo.db.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by zer0, the Maverick Hunter
 * on 22/07/18.
 * Class: Otp.java
 */
@Document(collection = "alfagift_otp")
public class Otp extends BaseDomain {

    private Long otpCode;
    private Date validUntil;
    private String phoneNum;
    private String action;
    private String message;

    public Otp(){}

    public Long getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(Long otpCode) {
        this.otpCode = otpCode;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
