package com.alfamart.alfagift_bo.db.mongo.domain;

import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

/**
 * Created by zer0, the Maverick Hunter
 * on 26/07/18.
 * Class: Location.java
 */
public class Location {
    private GeoJsonPoint coordinate;

    public Location(GeoJsonPoint coordinate) {
        this.coordinate = coordinate;
    }

    public GeoJsonPoint getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(GeoJsonPoint coordinate) {
        this.coordinate = coordinate;
    }
}
