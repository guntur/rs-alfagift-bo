package com.alfamart.alfagift_bo.db.mongo.domain;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by zer0, the Maverick Hunter
 * on 24/07/18.
 * Class: BaseDomain.java
 */
public class BaseDomain implements Serializable {

    @Id
    private String id;

    @JsonIgnore
    public Date createdAt;

    @JsonIgnore
    public Date updatedAt;
    private Integer createdBy;
    private Integer UpdatedBy;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        UpdatedBy = updatedBy;
    }
}
