package com.alfamart.alfagift_bo.db.mongo.repository;

import com.alfamart.alfagift_bo.db.mongo.domain.Reward;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by zer0, the Maverick Hunter
 * on 27/07/18.
 * Class: RewardRepository.java
 */
public interface RewardRepository extends MongoRepository<Reward, String> {
}
