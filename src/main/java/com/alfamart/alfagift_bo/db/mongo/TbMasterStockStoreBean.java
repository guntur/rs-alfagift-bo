package com.alfamart.alfagift_bo.db.mongo;

import org.bson.types.ObjectId;

import java.util.Date;

public class TbMasterStockStoreBean {
    private ObjectId oid;
    private Date createDate;
    private Date updateDate;
    private Integer createId;
    private Integer updateId;
    private String skuSeller;
    private String storeId;
    private Date eod;
    private String tagProd;
    private String active;
    private Integer stockAvaibility;

    public ObjectId getOid() {
        return oid;
    }

    public void setOid(ObjectId oid) {
        this.oid = oid;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getCreateId() {
        return createId;
    }

    public void setCreateId(Integer createId) {
        this.createId = createId;
    }

    public Integer getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Integer updateId) {
        this.updateId = updateId;
    }

    public String getSkuSeller() {
        return skuSeller;
    }

    public void setSkuSeller(String skuSeller) {
        this.skuSeller = skuSeller;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public Date getEod() {
        return eod;
    }

    public void setEod(Date eod) {
        this.eod = eod;
    }

    public String getTagProd() {
        return tagProd;
    }

    public void setTagProd(String tagProd) {
        this.tagProd = tagProd;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Integer getStockAvaibility() {
        return stockAvaibility;
    }

    public void setStockAvaibility(Integer stockAvaibility) {
        this.stockAvaibility = stockAvaibility;
    }
}
