package com.alfamart.alfagift_bo.servlet;


import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

import com.alfamart.alfagift_bo.v1.base.util.Properties;
import com.alfamart.alfagift_bo.db.postgresql.Manager;

public class LoadOnStartupServlet extends HttpServlet {
	
	private final Logger log = Logger.getLogger(this.getClass());
	private static final long serialVersionUID = -8711338131280256267L;

	public LoadOnStartupServlet() {

        try {

        	log.info("REST ENVIRONTMENT : " + System.getProperty("environtment"));
			log.info("REST LOG4J : " + System.getProperty("log4j.configuration"));
			log.info("POSTGRESQL.JDBC.DRIVER : " + Properties.getString("postgresql.jdbc.driver"));
			log.info("POSTGRESQL.JDBC.URL : " + Properties.getString("postgresql.jdbc.url"));
			log.info("POSTGRESQL.JDBC.USERNAME : " + Properties.getString("postgresql.jdbc.username"));
			log.info("POSTGRESQL.JDBC.PASSWORD : " + Properties.getString("postgresql.jdbc.password"));
			log.info("POSTGRESQL.JDBC.MAXTOTAL : " + Properties.getString("postgresql.jdbc.maxtotal"));
			
			BasicDataSource bdSourcePostgresql = new BasicDataSource();
			bdSourcePostgresql.setDriverClassName(Properties.getString("postgresql.jdbc.driver"));
			bdSourcePostgresql.setUrl(Properties.getString("postgresql.jdbc.url"));
			bdSourcePostgresql.setUsername(Properties.getString("postgresql.jdbc.username"));
			bdSourcePostgresql.setPassword(Properties.getString("postgresql.jdbc.password"));
			bdSourcePostgresql.setMaxTotal(Integer.parseInt(Properties.getString("postgresql.jdbc.maxtotal")));
	        Manager.getInstance().setDataSource(bdSourcePostgresql);
	        
//	        log.info("TEST LOAD DB : " + (TbMasterProvinsiManager.getInstance().countAll()));

        } catch (Exception ex) {
        	ex.printStackTrace();
        }
	}
}
