/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.service;

import com.alfamart.alfagift_bo.v1.product.model.GetProductRequest;
import com.alfamart.alfagift_bo.v1.product.process.ProductProcess;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.alfamart.alfagift_bo.v1.base.model.Header;

import java.util.Map;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Elko
 */
@Service("productServiceV1")
public class ProductServiceV1Impl implements ProductServiceV1{
    
    private Gson gson = new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd").create();    
    
    @Override
    public Response root() {
        return Response.ok("REST ALFAMIND TO PRODUCT API").build();
    }
    
    public Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    ProductProcess productProcess;
    @Override
    public Map<String,Object> getProducts(Header header, GetProductRequest request) {
        return productProcess.getProducts(header, request, 602);
    }

    @Override
    public Map<String,Object> getVirtualProducts(Header header, GetProductRequest request) {
        return productProcess.getProducts(header, request, 215);
    }
    
}
