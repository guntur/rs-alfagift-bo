package com.alfamart.alfagift_bo.service;

import com.alfamart.alfagift_bo.base.exception.CustomException;
import com.alfamart.alfagift_bo.base.exception.ServiceException;
import com.alfamart.alfagift_bo.v1.base.model.BaseResponse;
import com.alfamart.alfagift_bo.v1.base.util.Constant;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;
import com.alfamart.alfagift_bo.v1.interest.model.InterestModel;
import com.alfamart.alfagift_bo.v1.interest.service.InterestService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zer0, the Maverick Hunter
 * on 06/08/18.
 * Class: InterestServiceImpl.java
 */
@Service("interestServiceV1")
public class InterestServiceV1Impl implements InterestServiceV1 {

    public Logger logger = LoggerFactory.getLogger(this.getClass());
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private final InterestService interestService;

    @Autowired
    public InterestServiceV1Impl(InterestService interestService) {
        this.interestService = interestService;
    }

    @Override
    public String root() {
        return "REST ALFAGIFT BO FOR Interest Module";
    }

    @Override
    public BaseResponse getInterest(Integer limit, Integer offset, String keyword, String id) throws ServiceException, CustomException {

        logger.info("ALFAGIFT-BO - getInterest() request : " + limit + "," + offset + "," + keyword);
        BaseResponse response = new BaseResponse();

        try {

            response.setStatus(1);

            if (ValidatorUtil.isNullOrEmpty(id)) {

                List<InterestModel> brandPromotions = interestService.getMasterInterest();

                if (brandPromotions.size() > 0) {
                    response.setData(brandPromotions);
                    response.setMessage(Constant.ResponseMessage.SUCCESS);
                }
            } else {

                InterestModel interestModel = interestService.getInterest(id);

                if (!ValidatorUtil.isNullOrEmpty(interestModel)) {
                    response.setData(interestModel);
                    response.setMessage(Constant.ResponseMessage.SUCCESS);
                } else {
                    response.setMessage(Constant.ResponseMessage.EMPTY_RESULT);
                }
            }

        } catch (MongoException e) {
            response.setStatus(0);
            response.setMessage(e.getMessage());
        }

        logger.info("ALFAGIFT-BO - getInterest() response : " + gson.toJson(response));

        return response;
    }

}
