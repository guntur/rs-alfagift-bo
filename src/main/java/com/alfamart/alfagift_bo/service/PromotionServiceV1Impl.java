package com.alfamart.alfagift_bo.service;

import com.alfamart.alfagift_bo.base.exception.CustomException;
import com.alfamart.alfagift_bo.base.exception.ServiceException;
import com.alfamart.alfagift_bo.db.mongo.domain.Brand;
import com.alfamart.alfagift_bo.v1.base.model.BaseResponse;
import com.alfamart.alfagift_bo.v1.base.util.Constant;
import com.alfamart.alfagift_bo.v1.base.util.ValidatorUtil;
import com.alfamart.alfagift_bo.v1.promotion.model.BrandPromotion;
import com.alfamart.alfagift_bo.v1.promotion.service.PromotionManagementService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zer0, the Maverick Hunter
 * on 02/08/18.
 * Class: PromotionServiceV1Impl.java
 */
@Service("promotionServiceV1")
public class PromotionServiceV1Impl implements PromotionServiceV1 {

    public Logger logger = LoggerFactory.getLogger(this.getClass());
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private final PromotionManagementService promotionManagementService;

    @Autowired
    public PromotionServiceV1Impl(PromotionManagementService promotionManagementService) {
        this.promotionManagementService = promotionManagementService;
    }

    @Override
    public String root() {
        return "REST ALFAGIFT BO FOR Promotion Module";
    }

    @Override
    public BaseResponse getBrandPromotion(Integer limit, Integer offset, String keyword, String id) throws ServiceException, CustomException {

        logger.info("ALFAGIFT-BO - getBrandPromotion() request : " + limit + "," + offset + "," + keyword);
        BaseResponse response = new BaseResponse();

        try {

            response.setStatus(1);

            if (ValidatorUtil.isNullOrEmpty(id)) {

                List<BrandPromotion> brandPromotions = promotionManagementService.getBrandList();

                if (brandPromotions.size() > 0) {
                    response.setData(brandPromotions);
                    response.setMessage(Constant.ResponseMessage.SUCCESS);
                }
            } else {

                BrandPromotion brandPromotion = promotionManagementService.getBrand(id);

                if (!ValidatorUtil.isNullOrEmpty(brandPromotion)) {
                    response.setData(brandPromotion);
                    response.setMessage(Constant.ResponseMessage.SUCCESS);
                } else {
                    response.setMessage(Constant.ResponseMessage.EMPTY_RESULT);
                }
            }

        } catch (MongoException e) {
            response.setStatus(0);
            response.setMessage(e.getMessage());
        }

        logger.info("ALFAGIFT-BO - getBrandPromotion() response : " + gson.toJson(response));

        return response;
    }

    @Override
    public BaseResponse saveBrandPromotion(String id, BrandPromotion brandPromotion) throws ServiceException, CustomException {

        logger.info("ALFAGIFT-BO - saveBrandPromotion() request : " + gson.toJson(brandPromotion));
        BaseResponse response = new BaseResponse();

        try {

            if (!ValidatorUtil.isNullOrEmpty(id)) {
                brandPromotion.setBrandId(id);
            }

            Brand brand = promotionManagementService.saveUpdateBrand(brandPromotion, brandPromotion.getCreatedBy());
            BrandPromotion brandPromotionResponse = new BrandPromotion(brand);

            response.setData(brandPromotionResponse);
            response.setMessage(Constant.ResponseMessage.SUCCESS);
            response.setStatus(1);

        } catch (MongoException e) {
            response.setStatus(0);
            response.setMessage(e.getMessage());
        }

        logger.info("ALFAGIFT-BO - saveBrandPromotion() response : " + gson.toJson(response));

        return response;
    }
}
