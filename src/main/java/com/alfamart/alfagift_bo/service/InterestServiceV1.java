package com.alfamart.alfagift_bo.service;

import com.alfamart.alfagift_bo.base.exception.CustomException;
import com.alfamart.alfagift_bo.base.exception.ServiceException;
import com.alfamart.alfagift_bo.v1.base.common.RestUrl;
import com.alfamart.alfagift_bo.v1.base.model.BaseResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by zer0, the Maverick Hunter
 * on 06/08/18.
 * Class: InterestServiceV1.java
 */
@Path(RestUrl.V1.INTEREST_ROOT)
@Api(value = "Interest Service")
public interface InterestServiceV1 {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Root Context",
            response = String.class,
            responseContainer = "String")
    public String root();

    @GET
    @Path(RestUrl.V1.INTEREST_GET)
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public BaseResponse getInterest(
            @QueryParam(value = "limit") Integer limit,
            @QueryParam(value = "offset") Integer offset,
            @QueryParam(value = "keyword") String keyword,
            @QueryParam(value = "id") String id
    ) throws ServiceException, CustomException;

}
