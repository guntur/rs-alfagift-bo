/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alfamart.alfagift_bo.service;

import java.util.Map;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.alfamart.alfagift_bo.v1.base.common.RestUrl;
import com.alfamart.alfagift_bo.v1.base.model.Header;
import com.alfamart.alfagift_bo.v1.product.model.GetProductRequest;

/**
 *
 * @author Elko
 */
public interface ProductServiceV1 {
   
    @GET
    @Path(RestUrl.V1.PRODUCT_ROOT)
    @Produces(MediaType.APPLICATION_JSON)
    public Response root();
    
    @GET
    @Path(RestUrl.V1.PRODUCT_GET)
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Map<String,Object> getProducts(@BeanParam Header header,@BeanParam GetProductRequest request);
    
    @GET
    @Path(RestUrl.V1.PRODUCT_VIRTUAL_GET)
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Map<String,Object> getVirtualProducts(@BeanParam Header header,@BeanParam GetProductRequest request);
}
