package com.alfamart.alfagift_bo.service;

import com.alfamart.alfagift_bo.base.exception.CustomException;
import com.alfamart.alfagift_bo.base.exception.ServiceException;
import com.alfamart.alfagift_bo.v1.base.common.RestUrl;
import com.alfamart.alfagift_bo.v1.base.model.BaseResponse;
import com.alfamart.alfagift_bo.v1.promotion.model.BrandPromotion;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by zer0, the Maverick Hunter
 * on 20/07/18.
 * Class: OTPServiceV1.java
 */
@Path(RestUrl.V1.PROMOTION_ROOT)
@Api(value = "Promotion Service")
public interface PromotionServiceV1 {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Root Context",
            response = String.class,
            responseContainer = "String")
    public String root();

    @GET
    @Path(RestUrl.V1.BRAND_GET)
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public BaseResponse getBrandPromotion(
            @QueryParam(value = "limit") Integer limit,
            @QueryParam(value = "offset") Integer offset,
            @QueryParam(value = "keyword") String keyword,
            @QueryParam(value = "id") String id
    ) throws ServiceException, CustomException;

    @POST
    @Path(RestUrl.V1.BRAND_SAVE)
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public BaseResponse saveBrandPromotion(
            @QueryParam(value = "brandId") String id,
            @RequestBody BrandPromotion brandPromotion
    ) throws ServiceException, CustomException;

}
